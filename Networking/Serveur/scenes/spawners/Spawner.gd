extends Spatial

#### Attributs ####
onready var spawn_position = $SpawnPosition

func spawn(item_to_spawn):
	var instance = item_to_spawn.instance()
	instance.transform.origin = get_spawn_global_position()
	#instance.set_name(str(get_name()) + "G")
	instance.set_name(str(get_name()) + "G_" + str(instance.item_name))
	get_parent().add_child(instance)
	#get_tree().current_scene.add_child(instance)
	return [instance.get_filename(), get_parent().get_path(), instance.get_name(), [0,get_spawn_global_position()]]

#### Getter ####
func get_spawn_global_position():
	return spawn_position.global_transform.origin
