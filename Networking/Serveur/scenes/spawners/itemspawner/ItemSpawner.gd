extends "res://scenes/spawners/Spawner.gd"

#### Attributs ####
export var items_odds = {}

func spawn_random():
	# Choix d'un item aléatoire en respectant les probabilités de chaque item
	randomize()
	var random_number = randf()
	var item_to_spawn
	var i = items_odds.keys().size()
	for item in items_odds.keys():
		i -= 1
		if item != null:
			random_number -= items_odds[item]
			if random_number <= 0 or i<=0:
				item_to_spawn = item
				break
	return spawn(item_to_spawn)
