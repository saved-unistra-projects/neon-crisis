extends KinematicBody

#### Constantes ####
# Vitesse de saut
const JUMP_SPEED = 10
# Pente max de ce que l'on condière un "escalier" (pour exclure les murs)
const STAIRS_MAX_SLOPE = 20

#### Attributs ####
# Points de vie
var health = 100

# Vitesse
var speed = 15

# Direction et velocité
var direction = Vector3.ZERO
var velocity = Vector3.ZERO

# Permet de récupérer le vecteur de la dernière collision
var last_collision_vector : Vector3

# Sensibilité de la souris
var mouse_sensitivity = 0.05

# Caméra, pivot, et référence pour l'arme
onready var camera = $Pivot/Camera
onready var pivot = $Pivot
onready var weapon_reference = $Pivot/WeaponReference

# Particules de sang
onready var blood = preload("particles/Blood.tscn")

# Raycast pour les escaliers
onready var stair_ray_casts = $StairRayCasts
onready var ray_cast_checker = $RayCastChecker

var ray_length = 1000
#### Armes ####
# Tableau de NodePath représentant l'inventaire
export var inventory = []
var equipped_weapon = null
var equipped_weapon_index = -1

func can_send_position_to(pfrom, pto):
	if pfrom == pto or pto == 0:
		return false
	#else
	var camera = get_tree().get_root().get_node("NetworkManager/Players/" + str(pto)).get_node("Pivot/Camera")
	var posfrom = get_tree().get_root().get_node("NetworkManager/Players/" + str(pfrom)).translation
	
	if(camera.is_position_behind(posfrom)):
		return false
		
	var posto = get_tree().get_root().get_node("NetworkManager/Players/" + str(pto)).translation
	var space_state = get_world().direct_space_state
	var result = space_state.intersect_ray(posto, posfrom)
	
	if result:
		return int(result.collider.name) == pfrom
	
	return false
	
remote func update_trans_rot(pos, rot, head_rot, id_j):
	get_tree().get_root().get_node("NetworkManager/Players/" + str(id_j)).translation = pos
	get_tree().get_root().get_node("NetworkManager/Players/" + str(id_j)).rotation = rot
	get_tree().get_root().get_node("NetworkManager/Players/" + str(id_j)).get_node("Pivot/Camera").rotation = head_rot

	var tab = get_tree().get_root().get_node("NetworkManager").all_players
	for id in tab:
		if can_send_position_to(id_j, id):
			rpc_unreliable_id(id, "update_trans_rot", pos, rot, head_rot, id_j)

# Pour créer un Node dans la scene Serveur, à la demande d'un client
remote func insertNode(nodeFilename, path, nodeName, caller_id, typeNode):
	print("Insert node " + str(nodeName) + " into :" + str(caller_id) + " with path: " + str(path))
	var node = load(nodeFilename).instance()
	node.set_name(nodeName)
	get_node(path).add_child(node)
	
	# Maintenant on envoie cette info à tous les autres joueurs, pour les synchro
	var tab = get_tree().get_root().get_node("NetworkManager").all_players
	for id in tab:
		if id!=caller_id:
			#print("Call " + str(id))
			rpc_id(id, "insertNode", nodeFilename, path, nodeName, caller_id, typeNode)

remote func change_weapon(path, weapName, action, caller_id):
	var tab = get_tree().get_root().get_node("NetworkManager").all_players
	for id in tab:
		if id!=caller_id:
			#print("Call2 " + str(id))
			rpc_id(id, "change_weapon", path, weapName, action, caller_id)
