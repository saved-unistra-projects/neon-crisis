extends CPUParticles

var timer : Timer

func _ready():
	timer = $Timer
	timer.start(lifetime)
	timer.connect("timeout",self,"queue_free")
