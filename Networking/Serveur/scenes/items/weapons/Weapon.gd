tool
extends "res://scenes/items/Item.gd"
#warning-ignore-all:unused_class_variable
#warning-ignore:return_value_discarded

#### Attributs ####
export var weapon_max_range = 1000
export var max_ammo : int = 20
export var max_magazine_size = 10
var ammo : int
var magazine_size : int

var is_reloading = false

# Timer pour cooldown
onready var timer = $Timer

#### Outils editeur ####
export var preview_player : bool = false setget set_preview_player
var player_scene : PackedScene = preload("res://scenes/characters/player/Player.tscn")
var player_node
var camera_node

#### Setters ####
		
func set_preview_player(b):
	if Engine.editor_hint:
		preview_player = b
		if preview_player:
			player_node = player_scene.instance()
			add_child(player_node)
			player_node.name = "PreviewPlayer"
			player_node.owner = get_tree().edited_scene_root
			camera_node = player_node.get_node("Pivot/Camera").duplicate()
			add_child(camera_node)	
			camera_node.global_transform = player_node.get_node("Pivot/Camera").global_transform
			camera_node.name = "PreviewCamera"
			camera_node.owner = get_tree().edited_scene_root
			
		else:
			if player_node != null:
				if camera_node != null:
					camera_node.queue_free()
					camera_node = null
				player_node.queue_free()
				
				player_node = null
	
func _ready():
	ammo = max_ammo
	magazine_size = max_magazine_size

func fire(_fire_point, _fire_vector,_info):
	# On ne tire que si le cooldown est terminé
	if timer.is_stopped() and magazine_size > 0:
		magazine_size -= 1
		if animation_player.current_animation == "fire":
			animation_player.stop()
		animation_player.play("fire")
		process_fire(_fire_point,_fire_vector,_info)
		timer.start()
		return true
	return false

func reload():
	if ammo>0 and animation_player.current_animation == "":
		animation_player.play("reload")

func effectively_reload(animation_finished):
	if animation_finished == "reload":
		var to_add = max_magazine_size - magazine_size
		if ammo >= to_add:
			ammo -= to_add
			magazine_size += to_add
		else:
			ammo = 0
			magazine_size += ammo

func add_ammo(amount: int ):
	ammo += amount
	ammo = clamp(ammo, 0, max_ammo)
		
# Fonction qui indique que faire lorsque que l'on tire
func process_fire(_fire_point,_fire_vector,_info):
	# Fonction "virtuelle"
	pass
		
	
func cast_ray(point, normal) -> Dictionary:
		# Récupération du point d'origine
		var from = point
		# Récupération du point d'arrivée (avec le vecteur donné)
		var to = from + normal * weapon_max_range
		var space_state = get_world().direct_space_state
		# Récupération de la collision
		return space_state.intersect_ray(from,to)
	
