extends "res://scenes/items/weapons/projectile_weapons/projectiles/Projectile.gd"

#### Attribut ####
onready var explosion_area = $ExplosionArea

func _on_body_shape_entered(_body_id, _body, _body_shape, _local_shape):
	process_object_hit()
	if explosion_area.monitoring:
		for body_hit in explosion_area.get_overlapping_bodies():
			process_explosion_hit(body_hit)
		explosion_area.monitoring  = false
		
func process_explosion_hit(_body):
	# Fonction "virtuelle"
	pass

func process_object_hit():
	# Fonction "virtuelle"
	pass
