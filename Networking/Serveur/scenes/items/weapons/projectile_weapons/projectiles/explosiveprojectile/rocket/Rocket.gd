extends "res://scenes/items/weapons/projectile_weapons/projectiles/explosiveprojectile/ExplosiveProjectile.gd"

#### Attribut ####
export var damage = 40
export var knock_back_force = 10
onready var explosion_particles = $ExplosionParticles
onready var timer = $ExplosionParticles/Timer

func process_explosion_hit(body):
	if body.has_method("take_hit"):
		body.take_hit(damage,player_info,false)
		var knock_back_vector = body.global_transform.origin - global_transform.origin
		knock_back_vector = knock_back_vector.normalized()
		if body.has_method("knock"):
			body.knock(knock_back_vector * knock_back_force)
	
func process_object_hit():
	$Model.hide()
	if timer.is_stopped():
		timer.start(explosion_particles.lifetime)
	explosion_particles.emitting = true
