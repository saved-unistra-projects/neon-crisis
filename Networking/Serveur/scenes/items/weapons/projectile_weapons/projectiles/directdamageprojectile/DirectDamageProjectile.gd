extends "res://scenes/items/weapons/projectile_weapons/projectiles/Projectile.gd"

#warning-ignore-all:unused_argument

#### Attributs ####
export var head_damage = 100
export var body_damage = 20

func _on_body_shape_entered(body_id, body, body_shape, local_shape):
	._on_body_shape_entered(body_id, body, body_shape, local_shape)
	queue_free()

func player_head_hit(body_id, body, body_shape, local_shape):
	if body.has_method("take_hit"):
		body.take_hit(head_damage,player_info,translation)
		# Point de sang à changer (translation pas suffisant)

func player_body_hit(body_id, body, body_shape, local_shape):
	if body.has_method("take_hit"):
		body.take_hit(body_damage,player_info,translation)
