tool
extends "../Weapon.gd"

const TREE_WROLD = "NetworkManager/TeamDeathmatchManager/World"
#### Attributs ####
# Projectile à instancier
export var projectile_scene : PackedScene setget set_projectile_scene
# Référence pour le placement des projectiles
onready var projectile_reference = $Movables/ProjectileReference
# Preview projectile bool
export var preview_projectile : bool = false setget set_preview_projectile
# Preview du projectile
var preview_projectile_node


func _get_configuration_warning() -> String:
	if projectile_scene == null:
		return "No projectile attached"
	else:
		return ''

func _ready():
	pass

func set_projectile_scene(new_projectile):
	projectile_scene = new_projectile
	#if Engine.editor_hint:
		#if get_tree() != null:
	#	get_tree().emit_signal("node_configuration_warning_changed", self)

func process_fire(fire_point, fire_vector,info):
	var result = cast_ray(fire_point,fire_vector)
	var target_point = Vector3.ZERO
	if result.empty():
		target_point = fire_point + fire_vector * weapon_max_range
	else:
		target_point = result.position
	var projectile = projectile_scene.instance()
	projectile.transform = projectile_reference.global_transform
	projectile.set_player_info(info)
	#get_tree().current_scene.add_child(projectile)
	get_tree().get_root().get_node(TREE_WROLD).add_child(projectile)
	var vector = target_point - projectile.translation
	if projectile.has_method("add_central_force"):
		var projectile_force = 1
		if "projectile_force" in projectile:
			projectile_force *= projectile.projectile_force
		projectile.add_central_force(vector.normalized() * projectile_force)
	
func set_preview_projectile(b):
	if Engine.editor_hint:
		if projectile_scene != null:
			preview_projectile = b
			if preview_projectile:
				preview_projectile_node = projectile_scene.instance()
				projectile_reference.add_child(preview_projectile_node)
				preview_projectile_node.owner = get_tree().edited_scene_root
			else:
				if preview_projectile_node != null:
					print(preview_projectile_node)
					preview_projectile_node.free()
					preview_projectile_node = null
		else:
			print("Error : no projectile scene attached")
		
		
