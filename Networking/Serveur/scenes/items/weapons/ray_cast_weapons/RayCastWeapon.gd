tool
extends "../Weapon.gd"

#### Attributs #####
# Dégâts dans la tête
export var head_damage = 100
# Dégâts dans le reste du corps
export var body_damage = 20

func process_fire(fire_point, fire_vector,info):
	var result = cast_ray(fire_point, fire_vector)
	if not result.empty():
		if result.collider.is_in_group("Player"):
			var shape = result.collider.shape_owner_get_owner(result.shape)
			if shape.is_in_group("HeadPart"):
				result.collider.take_hit(head_damage,info,true, result.position)
			elif shape.is_in_group("BodyPart"):
				result.collider.take_hit(body_damage,info,true, result.position)
