extends KinematicBody

#### Constantes ####
# Vitesse de saut
const JUMP_SPEED = 10
# Pente max de ce que l'on condière un "escalier" (pour exclure les murs)
const STAIRS_MAX_SLOPE = 20

signal dead(info)
#### Attributs ####
# Points de vie
var health = 100

# Vitesse
var speed = 15

var player_team = {} setget set_player_team

# Direction et velocité
var direction = Vector3.ZERO
var velocity = Vector3.ZERO

# Permet de récupérer le vecteur de la dernière collision
var last_collision_vector : Vector3

# Sensibilité de la souris
var mouse_sensitivity = 0.05

# Caméra, pivot, et référence pour l'arme
onready var camera = $Pivot/Camera
onready var pivot = $Pivot
onready var weapon_reference = $Pivot/WeaponReference

# Particules de sang
onready var blood = preload("particles/Blood.tscn")

# Raycast pour les escaliers
onready var stair_ray_casts = $StairRayCasts
onready var ray_cast_checker = $RayCastChecker

var ray_length = 1000
#### Armes ####
# Tableau de NodePath représentant l'inventaire
export var inventory = []
var equipped_weapon = null
var equipped_weapon_index = -1
var player_info = {}
var last_hit_info

const TREE_WORLD = "NetworkManager/TeamDeathmatchManager/World"

var rng = RandomNumberGenerator.new()

var player_dead = false
func _ready():
	rng.randomize()

func set_player_info(info):
	player_info = info
	
func set_player_team(team):
	player_team = team

func can_send_position_to(pfrom, pto):
	if pfrom == pto or pto == 0:
		return false
	#else
	var player_from = str(NetworkManager.all_players[pfrom].spawn_path)+"/"+str(pfrom)
	var player_to = str(NetworkManager.all_players[pto].spawn_path)+"/"+str(pto)
	
	var cam = get_tree().get_root().get_node(player_to).get_node("Pivot/Camera")
	var posfrom = get_tree().get_root().get_node(player_from).translation
	
	if(cam.is_position_behind(posfrom)):
		return false
	
	var poshead = get_tree().get_root().get_node(player_to).get_node("HeadColllisionShape").translation
	var posto = get_tree().get_root().get_node(player_to).translation
	posto += poshead
	for node in get_tree().get_root().get_node(player_from).get_node("Model/CSGBox/VisibilityPoints").get_children():
		var tmp = node.translation
		tmp += posfrom
		var space_state = get_world().direct_space_state
		var result = space_state.intersect_ray(posto, tmp)
	
		if result:
			#print(result.collider.name)
			if int(result.collider.name) == pfrom:
				return true	
	return false

remote func update_trans_rot(pos, rot, head_rot):
	if player_dead:
		return
	translation = pos
	rotation = rot
	camera.rotation = head_rot
	var id_j = get_tree().get_rpc_sender_id()
	
	for id in NetworkManager.all_players:
		if can_send_position_to(id_j, id):
			rpc_unreliable_id(id, "update_trans_rot", pos, rot, head_rot)
		else:
			if id != id_j and rng.randi_range(0, 40) == 0:
				rpc_unreliable_id(id, "hide_player")

remote func set_equipped_weapon(path_idx):
	if path_idx[1]==-1:
		if equipped_weapon!=null:
			equipped_weapon.hide()
		equipped_weapon = null
	else:
		inventory[path_idx[1]].show()
		equipped_weapon = inventory[path_idx[1]]

remote func add_weap_to_inventory(nodeFilename, path, nodeName):
	var weap = load(nodeFilename).instance()
	weap.set_name(nodeName)
	weap.is_on_ground = false
	weap.is_pickable = false
	#weap.show()
	weap.global_transform = weapon_reference.global_transform
	get_node(path).add_child(weap)
	inventory.append(weap)
	print("Add_weap_to_inventory OKK  " + str(len(inventory)))

# Normalement non utilisé
remote func add_weapon_to_inventory_with_path(weapon_path):
	var weapon = get_tree().get_root().get_node(weapon_path)
	pivot.add_child(weapon)
	weapon.hide()
	weapon.global_transform = weapon_reference.global_transform
	inventory.append(weapon)

remote func fire(_fire_point, _fire_vector, _info):
	var id_j = get_tree().get_rpc_sender_id()
	
	for id in NetworkManager.all_players:
		if id != 0 and id_j != id:
			rpc_id(id, "update_trans_rot", translation, rotation, camera.rotation)
	
	var ret = false
	if equipped_weapon != null:#normalement != null 
		ret = equipped_weapon.fire(_fire_point, _fire_vector, _info)

	if ret:
		for id in NetworkManager.all_players:
			if id!=0 and id!=id_j:
				rpc_id(id, "fire", _fire_point, _fire_vector, _info)

func take_hit(damage, info, blood_bool=true, hit_position=Vector3(0,0,0)):
	print('la')
	last_hit_info = info
	# Changer la vie ...
	# Si le tir vient de mon équipe, je ne prend pas de dégâts
	if info!=null and info["team"] == player_info["team"]:
		# Sauf si il vient de moi, je prend les moitié des dégâts
		if info["username"] == player_info["username"]:
			set_health(health - damage/2)
	else:
		set_health(health - damage)
	
	print(health)


remote func set_health(new_health):
	health = new_health
	rpc("set_health", new_health)
	if health <= 0:
		die(player_info)
		rpc("die", player_info)

func die(info):
	player_dead = true
	emit_signal("dead",info)
	
func rebirth():
	rpc("update_trans_rot", translation, rotation, camera.rotation)
	rpc("rebirth")
	health = 100
	player_dead = false

remote func reload():
	if equipped_weapon != null:
			if equipped_weapon.has_method("reload"):
				equipped_weapon.reload()

remote func drop(weapon_index):
	var weapon = equipped_weapon
	#var weapon = inventory[weapon_index]
	var position = weapon.global_transform.origin
	pivot.remove_child(weapon)
	#get_tree().current_scene.add_child(weapon)
	get_tree().get_root().get_node(TREE_WORLD).add_child(weapon)
	weapon.global_transform.origin = position
	weapon.global_transform.origin.x += 10
	inventory.remove(weapon_index)
	equipped_weapon_index = -1
	if weapon.has_method("drop"):
		weapon.drop()
	equipped_weapon = null
