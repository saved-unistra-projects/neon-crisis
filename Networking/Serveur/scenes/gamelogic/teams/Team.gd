extends Node

#### Attributs ####
export var team_name : String
export var player_scene : PackedScene
var score :int = 0
var need_to_spawn = []

func _ready():
	pass

func init_spawner_signal():
	for spawner in get_tree().get_nodes_in_group("PlayerSpawner"):
		if spawner.connect("just_freed", self, "spawner_available")!=OK:
			print("Erreur connect spawner")

func instance_player(pInfo):
	var instance = player_scene.instance()
	var spawner = pick_random_spawner()
	instance.set_name(str(pInfo.net_id))
	instance.set_player_team(team_name)
	instance.connect("dead", self, "process_death")
	if spawner != null:
		add_child(instance)
		var spawn_point = spawner.move_player_to_spawn(instance)
		NetworkManager.all_players[pInfo.net_id].spawn_point = spawn_point
		NetworkManager.all_players[pInfo.net_id].spawn_path = instance.get_parent().get_path()
		#print("@@ " + str(NetworkManager.all_players[pId].spawn_point) + " | " + str(NetworkManager.all_players[pId].spawn_path))
		return pInfo.net_id
	else:
		need_to_spawn.append(NetworkManager.all_players[pInfo.net_id])
		NetworkManager.all_players.erase(pInfo.net_id)
	return null

func pick_random_spawner():
	var spawners = get_tree().get_nodes_in_group("PlayerSpawner")
	var i = 0
	var chosen_one
	if spawners.size() >= 1:
		chosen_one = spawners[i]
	while not chosen_one.is_free() and i!=(spawners.size()-1):
		i+=1
		chosen_one = spawners[i%spawners.size()]
	if not chosen_one.is_free():
		return null
	else:
		return chosen_one

func spawner_available(spawner):
	if need_to_spawn.size()>0:
		var pId = need_to_spawn[0].net_id
		var instance = player_scene.instance()
		instance.set_name(str(pId))
		instance.set_player_team(team_name)
		instance.connect("dead", self, "process_death")
		add_child(instance)
		var spawn_point = spawner.move_player_to_spawn(instance)
		NetworkManager.all_players[pId] = need_to_spawn[0]
		NetworkManager.all_players[pId].spawn_point = spawn_point
		NetworkManager.all_players[pId].spawn_path = instance.get_parent().get_path()
		#NetworkManager.send_spawn_one_player(pId)
		NetworkManager.send_spawning(pId)
		need_to_spawn.remove(0)
		
func process_death(_info):
	get_parent().process_death(_info)
	
