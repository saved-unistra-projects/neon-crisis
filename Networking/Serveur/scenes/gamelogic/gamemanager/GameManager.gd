extends Node

#### Attributs ####
const WORLD_SCENE = "res://scenes/levels/testscene/World.tscn"
const RESPAWN_WEAP_DELAY = 35

# Temps du round
export var game_time : int
# Liste des joueurs (pseudos)
#export var players = []  => remplacé par NetworkManager.all_players[]
# Liste des spawns de joueurs
var player_spawners = []
# Liste des spawns d'objets
var item_spawners = []
# La carte
var map
# Timer
onready var timer = $Timer

func _get_configuration_warning():
	if get_parent() == null:
		return "This node must have a map as its only child"
	else:
		map = get_parent()
		return ""

func _ready():
	if NetworkManager.connect("spawn_one", self, "other_inits")!=OK:
		print("Erreur connect")
	
	if NetworkManager.connect("spawn_ite", self, "other_inits")!=OK:
		print("Erreur connect")
	map = load(WORLD_SCENE).instance()
	add_child(map)
	
	for team in get_tree().get_nodes_in_group("Team"):
		team.init_spawner_signal()
	
	# Initialisation des points de spawn des joueurs
	player_spawners = get_tree().get_nodes_in_group("PlayerSpawner").duplicate()
	item_spawners = get_tree().get_nodes_in_group("ItemSpawner").duplicate()
	spawn_items()
	
	var start_delay = Timer.new()
	start_delay.connect("timeout",self,"_on_resp_weap_time") 
	start_delay.set_name("RespWeap")
	start_delay.set_wait_time(RESPAWN_WEAP_DELAY)
	add_child(start_delay)
	start_delay.start()

func _on_resp_weap_time():
	print("Delay Finitoo")
	#get_node("RespWeap").queue_free()
	for e in NetworkManager.to_respawn_item:
		var save = e[1].spawn_random()
		NetworkManager.to_spawn.append( [save, e[1]] )
		NetworkManager.send_node(save[0], save[1], save[2], save[3])
		NetworkManager.to_respawn_item.erase(e)


func spawn_items():
	var to_spawn = NetworkManager.to_spawn
	for item_spawner in item_spawners:
		#item_spawner.call_deferred("spawn_random")
		to_spawn.append( [item_spawner.spawn_random(), item_spawner]  )
	
	for i in to_spawn:
		NetworkManager.send_node(i[0][0], i[0][1], i[0][2], i[0][3])

func other_inits(pInfo):
	# Fonction virtuelle
	pass
	
func spawn_players():
	# Fonction virtuelle
	pass

func start_timer():
	# Set l'interface
	timer.start(game_time)
	
func end_game():
	# Afficher l'écran de fin, appeler le réseau pour déconnecter les joueurs...
	pass

func _on_game_time_ended():
	end_game()

func process_death(_info):
	# Virtual function
	pass
