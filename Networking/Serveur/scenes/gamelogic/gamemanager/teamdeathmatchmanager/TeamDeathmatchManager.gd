extends "res://scenes/gamelogic/gamemanager/GameManager.gd"

#### Attributs ####
# But de points
export var goal : int
# Liste des équipes
export var team_list = []

const PLAYER_SPAWNER_PATH = "NetworkManager/TeamDeathmatchManager/World/PlayerSpawn"
#signal dead(info)

#func _ready():
#	if get_tree().connect("dead", self, "_on_player_dead")!=OK:
#		print("Erreur connect")

#func _on_player_dead(info):
#	print(info)

func _get_configuration_warning():
	#init_teams()
	if team_list.size() == 0:
		return "This node needs a team child node"
	else:
		return ""

func other_inits(pInfo):
		init_teams(pInfo)
	
func init_teams(pInfo):
	team_list = get_tree().get_nodes_in_group("Team")
	#var i = NetworkManager.all_players.size()
	#for id in NetworkManager.all_players:
	#	var team = team_list[i%team_list.size()]
	#	var spawned = team.instance_player(id)
		#if spawned==false:
		#	peut etre envoyer un message au client en question pour lui 
		#	indiquer qu'il doit un peu attendre avant de pouvoir spawn
	#	i -= 1
	var team = team_list[pInfo.team]
	var pId = team.instance_player(pInfo)
	if pId!=null:
		NetworkManager.send_spawning(pId)

func process_death(_info):
	var spawning_positions = get_tree().get_root().get_node(PLAYER_SPAWNER_PATH).get_children()
	var can_break = false
	while not can_break:
		for spawning_position in spawning_positions:
			if spawning_position.is_free():
				var player = get_tree().get_root().get_node(_info.spawn_path).get_node(str(_info.net_id))
				spawning_position.move_player_to_spawn(player)
				player.rebirth()
				can_break = true
