extends Node

const PORT = 5647
const MAX_PLAYERS = 8

signal spawn_one(pInfo)
signal spawn_ite()

const GAME_MANAGER = "res://scenes/gamelogic/gamemanager/teamdeathmatchmanager/TeamDeathmatchManager.tscn"
var all_players = {}
var tmp_team = 0
var to_spawn = []
var to_respawn_item = []

func _ready():
	if get_tree().get_root().get_node("MainScene") != null:
		get_tree().get_root().get_node("MainScene").queue_free()
	if get_tree().connect("network_peer_connected", self, "_on_client_connected")!=OK || \
	get_tree().connect("network_peer_disconnected", self, "_on_client_disconnected")!=OK:
		print("Erreur connect")
		return
	var server = NetworkedMultiplayerENet.new()
	if server.create_server(PORT, MAX_PLAYERS)!=OK:
		print("Echec creation du serveur")
		return
	get_tree().set_network_peer(server)
	
	var game_manger = load(GAME_MANAGER).instance()
	add_child(game_manger)
	print("Serveur crée")

func _on_client_connected(id):
	print("Client " + str(id) + " connected")

func _on_client_disconnected(id):
	print("Player " + all_players[id].name + " déconnecté")
	rpc("unregister_player", id)

remote func register_player(pInfo):
	pInfo["team"] = tmp_team % 2
	pInfo["username"] = tmp_team
	tmp_team += 1
	all_players[pInfo.net_id] = pInfo
	emit_signal("spawn_one", pInfo)
	
	for id in all_players:
		rpc_id(pInfo.net_id, "register_player", all_players[id]) 	# Envoie info de chaque joueurs déja connecté à notre new joueur
		rpc_id(id, "register_player", pInfo) 						# Envoie info de notre new joueur à tous les autres joueurs
	print("Enregistrement du Player " + pInfo.name + " (" + str(pInfo.net_id) + ")")

remotesync func unregister_player(id):
	print("Suppression du Player " + all_players[id].name + " (" + str(id) + ")")
	var pInfo = all_players[id]
	all_players.erase(id)
	for i in all_players:
		rpc_id(i, "despawn_player", pInfo)
	despawn_player(pInfo)

func send_spawning(pId):
	for e in to_spawn:
		send_node_id(pId, e[0][0], e[0][1], e[0][2], e[0][3])
	
	rpc_id(pId, "spawn_player", all_players[pId]) # Instancier le nouveau joueur dans sa scène
	for others in all_players:
		if others!=pId:
			rpc_id(pId, "spawn_player", all_players[others]) # Instancier les autres joueurs dans la scène du nouveau joueur
			rpc_id(others, "spawn_player", all_players[pId]) # Instancier le new joueur dans la scène des autres joueurs

	var pInfo = all_players[pId]
	var player_node = get_tree().get_root().get_node(str(pInfo.spawn_path)+"/"+str(pInfo.net_id))
	player_node.set_player_info(pInfo)

#func send_spawn_one_player(pId):
#	rpc_id(pId, "spawn_player", all_players[pId]) # Instancier le nouveau joueur dans sa scène
#	for others in all_players:
#		if others!=pId:
#			rpc_id(pId, "spawn_player", all_players[others]) # Instancier les autres joueurs dans la scène du nouveau joueur
#			rpc_id(all_players[others].net_id, "spawn_player", all_players[pId]) # Instancier le nouveau joueur dans la scène des autres joueurs

func despawn_player(pInfo):
	print("Despawn_player")
	var player_node = get_tree().get_root().get_node(str(pInfo.spawn_path)+"/"+str(pInfo.net_id))
	if !player_node:
		print("Erreur: noeud invalide dans l'arbre " + str(player_node))
		return
	player_node.queue_free()

func send_node_id(pId, nodeFilename, path, nodeName, type):
	if type[0]==0: # Send spawn_weapon | type[1]: translation of the weapon
		rpc_id(pId, "insert_node", nodeFilename, path, nodeName, type)

func send_node(nodeFilename, path, nodeName, type):
	if type[0]==0: # Send spawn_weapon | type[1]: translation of the weapon
		rpc("insert_node", nodeFilename, path, nodeName, type)

#NetworkManager.send_node(equipped_weapon.get_filename(), pivot.get_path(), equipped_weapon.get_name(), [2, get_tree().get_network_unique_id()])
remote func insert_node(nodeFilename, path, nodeName, type):
	print("Insert node Name:" + str(nodeFilename) + " Path:" + str(path) + " NodeName:" + str(nodeName) + " Type:" +str(type))
	
	#if (type[0]==1 or type[0]==2) and has_node(str(path)+"/"+str(nodeName)):
	#	print("Toz")
	#	return
	
	if type[0]==2:
		for id in NetworkManager.all_players:
			if id!=type[1]:
				rpc_id(id, "insert_node", nodeFilename, path, nodeName, type)
		return
		
	#var node = load(nodeFilename).instance()
	#node.set_name(nodeName)
	
	#if type[0]==1: # Take weapon on hand
	#	node.is_on_ground = false
	#	node.is_pickable = false
	#	node.show()
	#	node.translation = Vector3(0, -2.145, 0.615)
	#	node.rotation_degrees = Vector3(0, -180, 0)
	
	#get_node(path).add_child(node)
	
	#if type[0]==1: # Take weapon on hand => type[1]: Network unique ID
	#	for id in NetworkManager.all_players:
	#		if id!=type[1]:
	#			rpc_id(id, "insert_node", nodeFilename, path, nodeName, type)

remote func delete_node(path, caller_id, type):
	print("Delete node => path:" + str(path) + " caller_id:" + str(caller_id))
	if not has_node(path):
		print("Toz 2")
		return
	
	if type!=1:
		get_node(path).queue_free()

	for id in NetworkManager.all_players:
		if id!=caller_id:
			rpc_id(id, "delete_node", path, caller_id)
	
	for e in to_spawn:
		if str(e[0][1])+"/"+str(e[0][2])==str(path):
			to_respawn_item.append(e)
			to_spawn.erase(e)
			break

#NetworkManager.add_amo(item_name, get_tree().get_network_unique_id())
remote func add_ammo_into_weap(item_name, nbr_ammo, caller_id):
	for w in get_node(str(all_players[caller_id].spawn_path)+"/"+str(caller_id)).inventory:
		print("## " + str(w.item_name) + " " + str(item_name))
		if w.item_name == item_name:
			print("Avant : " + str(w.ammo))
			w.add_ammo(nbr_ammo)
			print("Après : " + str(w.ammo))
