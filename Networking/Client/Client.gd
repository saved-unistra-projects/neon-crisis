extends Node2D
#"91.121.81.74" 5647

var player_info = {
	name = "",
	net_id = 0,
	bdd_id = 0,
	instance_path = "res://scenes/characters/player/Player.tscn",
	remotePlayer_path = ["res://scenes/characters/player/remote/BlueRemotePlayer.tscn", "res://scenes/characters/player/remote/RedRemotePlayer.tscn"],
	spawn_point = "",
	spawn_path = ""
}

const GAME_MANAGER = "res://scenes/gamelogic/gamemanager/teamdeathmatchmanager/TeamDeathmatchManager.tscn"
var all_players_local = {}

func _ready():
	if get_tree().connect("connected_to_server", self, "_on_connected_to_server")!=OK || \
	get_tree().connect("connection_failed", self, "_on_connection_failed")!=OK || \
	get_tree().connect("server_disconnected", self, "_on_disconnected_from_server")!=OK:
		print("Erreur connect")

func join_server(ip, port):
	var game_manger = load(GAME_MANAGER).instance()
	add_child(game_manger)
	var network = NetworkedMultiplayerENet.new()
	if network.create_client(ip, port) != OK:
		print("Echec de création du client")
		return
	get_tree().set_network_peer(network)

func _on_connected_to_server():
	print('Client ' + str(player_info.net_id) + ' connecté au serveur')
	player_info.net_id = get_tree().get_network_unique_id()
	register_player(player_info)
	rpc_id(1, "register_player", player_info)

func _on_disconnected_from_server():
	print("Serveur déconnecté")
	get_node("TeamDeathmatchManager").queue_free()
	#get_tree().set_network_peer(null)
	all_players_local.clear()
	player_info.net_id = 0
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_connection_failed():
	print("Echec connection serveur")
	get_tree().set_network_peer(null)

remote func register_player(pInfo):
	print("Enregistrement du Player " + pInfo.name + " (" + str(pInfo.net_id) + ")")
	all_players_local[pInfo.net_id] = pInfo

remote func unregister_player(id):
	print("Suppression du Player " + all_players_local[id].name + " (" + str(id) + ")")
	all_players_local.erase(id)

#remote func start_init():
#	# En réalité, instance de TeamDeathMatchManager 
#	# => Si on veut plusieurs mode de jeu, on pourra simplment les faires passer en argument du exec du serv
#	var game_manger = load(GAME_MANAGER).instance()
#	add_child(game_manger)

remote func spawn_player(pInfo):
	print("Remote spawn_player")
	if pInfo.net_id == NetworkManager.player_info.net_id:
		spawn_me(pInfo)
	else:
		print("SPAWN ANOTHER PLAYER")
		var newPlayer = load(pInfo.remotePlayer_path[pInfo.team]).instance()
		newPlayer.set_name(str(pInfo.net_id))
		newPlayer.transform.origin = pInfo.spawn_point
		newPlayer.set_player_info(pInfo)
		NetworkManager.get_node(str(pInfo.spawn_path)).add_child(newPlayer)

func spawn_me(mInfo):
	print("SPWAN MEEE")
	var me = load(mInfo.instance_path).instance()
	me.set_name(str(mInfo.net_id))
	me.set_network_master(mInfo.net_id)
	me.transform.origin = mInfo.spawn_point
	me.set_player_info(mInfo)
	NetworkManager.get_node(str(mInfo.spawn_path)).add_child(me)

remote func despawn_player(pInfo):
	var player_node = get_tree().get_root().get_node(str(pInfo.spawn_path)+"/"+str(pInfo.net_id))
	if !player_node:
		print("Erreur: noeud invalide dans l'arbre " + str(player_node))
		return
	player_node.queue_free()

remote func insert_node(nodeFilename, path, nodeName, type):
	print("R-Insert node Name:" + str(nodeFilename) + " Path:" + str(path) + " NodeName:" + str(nodeName) + " Type:" +str(type))
	
	if (type[0]==1 or type[0]==2) and has_node(str(path)+"/"+str(nodeName)):
		print("Toz")
		return
	
	var node = load(nodeFilename).instance()
	node.set_name(nodeName)
	
	if type[0]==0: # Recv spawn_weapon | type[1]: translation of the weapon
		node.transform.origin = type[1]
		get_node(path).add_child(node)
	
	if type[0]==2: # Take weapon on hand => type[1]: Network unique ID
		node.is_on_ground = false
		node.is_pickable = false
		node.show()
		node.translation = Vector3(0, -2.145, 0.615)
		node.rotation_degrees = Vector3(0, -180, 0)
		get_node(path).add_child(node)

remote func delete_node(path, caller_id):
	if has_node(path):
		get_node(path).queue_free()
	#get_node(path).get_parent().remove_child(get_node(path))

func send_node(nodeFilename, path, nodeName, type):
	rpc_id(1, "insert_node", nodeFilename, path, nodeName, type)

func send_deleting_node(path, caller_id, type=0):
	rpc_id(1, "delete_node", path, caller_id, type)

func add_ammo_into_weap(item_name, nbr_ammo, caller_id):
	rpc_id(1, "add_ammo_into_weap", item_name, nbr_ammo, caller_id)
