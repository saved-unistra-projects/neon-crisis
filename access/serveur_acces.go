package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
	"context"
	"database/sql"
	"strconv"
	_ "github.com/go-sql-driver/mysql"
	"os/exec"
)

const (
	PORT = "127.0.0.1:5454"
	LAST_VERSION = "ALPHA"
	NB_PARTIES = 10
)

// faire les truc NON OK
// VOIR SI ON PEUT RATER JOIN LOBBY
const (
		AUTH = iota
		SIGN_UP
		CREATE_LOBBY
		JOIN_LOBBY
		DISCONNECT
		AUTH_OK
		AUTH_WRONG_VERSION
		AUTH_WRONG_LOGIN
		AUTH_WRONG_PWD
		AUTH_ALREADY_OPENED_SAME_ADSRESS
		AUTH_ALREADY_OPENED_SAME_LOGIN
		CREATE_PROFILE_OK
		CREATE_PROFILE_NOT_OK
		CREATE_LOBBY_OK
		CREATE_LOBBY_FULL
		CREATE_LOBBY_NAME_ALREADY_USED
)

type Partie struct {
	nom string
	port int
	nb_joueurs int
	nb_max int
}

type user struct {
	id int					// id de la bdd
	ip_address_port string	// addresse ip et port sur lequel un client est connécté
	login string
}

type users_connected struct {
	array_users [100]user
	nb_users_connected int	// nb de clients qui sont connéctés actuellement
}

/* créer et tester la connexion à la bdd
retour: pointeur sur une connexion ouverte de bdd */
func connexion_database() (db *sql.DB) {
	db, err := sql.Open("mysql", "Unistra-CMI1:cmi1-unistra@tcp(91.121.81.74:3306)/Unistra-CMI1")
	if err != nil {
		log.Fatal(err)
	}
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}
	return db
}

// ouvrir la connexion pour écouter les clients
func open_connexion() (l net.Listener) {
	l, err := net.Listen("tcp4", PORT)
	if err != nil {
		log.Fatal(err)
		return
	}
	return l
}

// fonction qui créé un nouveau profil dans la base de données
func create_profil(db *sql.DB, info []string, c net.Conn) int {
	rsql := "SELECT login, mdp FROM UTILISATEUR"
	ctx := context.Background()
	result := CREATE_PROFILE_OK
	rows, err := db.QueryContext(ctx, rsql)
    if err != nil {
        log.Fatal(err)
		return CREATE_PROFILE_NOT_OK
	}
	defer rows.Close()
	for rows.Next() {
		var name, password string
		err := rows.Scan(&name, &password)
		if err != nil {
			log.Fatal(err)
		}

		if name == info[1] {
			result = CREATE_PROFILE_NOT_OK
		}
	}

	if (result == CREATE_PROFILE_OK) {
		stmp, er := db.Prepare("INSERT INTO UTILISATEUR (login, mdp) VALUES (?, ?)")
		if er != nil {
			log.Fatal(er)
		}
		_, err := stmp.Exec(info[1], info[2])

		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Inscription d'un nouveau utilisateur %s\n",info[1])
	}

	if (result == CREATE_PROFILE_OK) {
		fmt.Printf("creation du compte %s réussie \n",info[1])
	} else {
		fmt.Printf("creation du compte %s refusée\n",info[1])
	}
	return result
}

// Fonction qui vérifie le login et le mot de passe d'un utilisateur
func check_user(temp string,db *sql.DB, id *int, login *string) int {
	*id = -1
	var result int = AUTH_WRONG_LOGIN
	request := strings.Split(temp, ":")

	ctx := context.Background()
	tsql := fmt.Sprintf("SELECT id_user, login, mdp FROM UTILISATEUR;")

	rows, err := db.QueryContext(ctx, tsql)
	if err != nil {
			log.Fatal(err)
	}

	defer rows.Close()

	for rows.Next() {
			var name, password string
			var id_local int

			err := rows.Scan(&id_local, &name, &password)
			if err != nil {
					log.Fatal(err)
			}

			if (name == request[1]) {
				if  (password == request[2]) {
					result = AUTH_OK
					*id = id_local
					*login = name
				} else { result = AUTH_WRONG_PWD }
			}

			//fmt.Printf("ID: %d, Name: %s, Mdp: %s, result : %d\n", id_local, name, password, result)

	}
	return result
}

//Fonction qui exécute une nouvelle instance d'un server Godot
func new_game(port int,nb_games *int,lobby *[NB_PARTIES]Partie){
	var position = *nb_games -1
	cmd := exec.Command("sleep", "1")
	err := cmd.Run()
	log.Printf("Partie %s port %d terminée code : %v", lobby[position].nom,lobby[position].port,err)
	for i:= position; i < NB_PARTIES-1; i++ {
		lobby[i].nom = lobby[i+1].nom
		lobby[i].port = lobby[i+1].port
		lobby[i].nb_joueurs = lobby[i+1].nb_joueurs
		lobby[i].nb_max = lobby[i+1].nb_max
	}
	*nb_games-=1
}

// teste si lobby avec le même nom existe déjà
func check_name_lobby(lobby *[NB_PARTIES]Partie,name string) bool {
		var result = true
		for i:= 0; i < NB_PARTIES; i++ {
			if lobby[i].nom == name {
				result=false
			}
		}
		return result
}

// Fonction qui créer et ajoute une partie dans la liste des parties
func create_lobby(temp string, lobby *[NB_PARTIES]Partie, nb_games *int, result *int) [NB_PARTIES]Partie {
	request := strings.Split(temp, ":")
	var err error
	if check_name_lobby(lobby,request[1]) {
		if *nb_games < NB_PARTIES {
			lobby[*nb_games].nom = request[1]
			lobby[*nb_games].port = 5455
			lobby[*nb_games].nb_joueurs = 0
			lobby[*nb_games].nb_max, err = strconv.Atoi(request[2])
			if err != nil {
				log.Fatal(err)
			}
			*nb_games+=1
			go new_game(lobby[*nb_games].port,nb_games,lobby)
			*result = CREATE_LOBBY_OK
			fmt.Printf("création de lobby %s port %d réussie\n",lobby[*nb_games-1].nom,lobby[*nb_games-1].port)
		} else {
			fmt.Printf("création de lobby %s port %d refusée : serveurs pleins \n",lobby[*nb_games-1].nom,lobby[*nb_games-1].port)
			*result = CREATE_LOBBY_FULL
		}
	} else {
		*result = CREATE_LOBBY_NAME_ALREADY_USED
		fmt.Printf("création de lobby %s port %d refusée : nom de serveur déja existant \n",lobby[*nb_games-1].nom,lobby[*nb_games-1].port)
	}

	return *lobby
}

//Fonction qui affiche la liste des parties en cours
func print_lobby(lobby *[10]Partie,nb_games *int) string {
	var result string
	if *nb_games == 0 {
		result = "Aucun salon disponible\n"
	}
	for i := 0; i < *nb_games; i++ {
		result += fmt.Sprintf("Partie %d : \n\t Nom : %s \n\t Port : %d \n\t Nombre de joueurs : %d \n\t Nombre max : %d \n",i,lobby[i].nom,lobby[i].port,lobby[i].nb_joueurs,lobby[i].nb_max)
	}
	return result
}

func print_users_connected(users* users_connected) {
	var result string
	for i := 0; i < users.nb_users_connected; i++ {
		result += fmt.Sprintf("Id : %d\n\t Login : %s \n\t",users.array_users[i].id, users.array_users[i].login)
	}
	fmt.Println(result)
}

//Fonction qui supprime un utilisateur de la liste des utilisateurs connectés
func delete_user(users* users_connected, ip_address string) {
	for i:= 0; i < users.nb_users_connected; i++ {
		if users.array_users[i].ip_address_port == ip_address {
			for j:=i; j < users.nb_users_connected - 1; j++ {
				users.array_users[i].id = users.array_users[i+1].id
				users.array_users[i].ip_address_port = users.array_users[i+1].ip_address_port
				users.array_users[i].login = users.array_users[i+1].login
			}
			users.nb_users_connected--
			break
		}
	}
}

// tester si un utilisateur est déjà connecté à cet adresse IP et à ce port spécifique
func test_connexion_open(users* users_connected, address_ip string) bool {
	var result = false
	for i:= 0; i < users.nb_users_connected; i++ {
		if users.array_users[i].ip_address_port == address_ip {
			result = true
			break
		}
	}
	return result
}

func test_already_logged(users* users_connected, login string) bool {
	var result = false
	for i:= 0; i < users.nb_users_connected; i++ {
		if users.array_users[i].login == login {
			result = true
			break
		}
	}
	return result
}

//Fonction qui permet de tester la version du client
func check_version(temp string) bool {
	var result = false
	request := strings.Split(temp, ":")
	if(request[3]==LAST_VERSION){
		result=true
	}
	return result
}

func disconnect_user(users* users_connected, ip_address string,c net.Conn) {
	delete_user(users, c.RemoteAddr().String())
	fmt.Println("Déconnexion de %s",ip_address)
	if users.nb_users_connected == 0 {
		fmt.Println("Aucun utilisateur n'est connecté sur le serveur")
	} else {
		for i := 0; i < users.nb_users_connected; i++ {
			fmt.Println(users.array_users[i].id, users.array_users[i].ip_address_port)
		}
	}
	print_users_connected(users)
}

// boucle infinie pour recevoir les messages des clients
func handle_connection(c net.Conn,db *sql.DB, lobby *[NB_PARTIES]Partie, nb_games *int, users* users_connected) {
	fmt.Printf("Connexion de %s\n", c.RemoteAddr().String())
	var result string
	var connect = true
	var id_client int
	var login string
 	for connect {
		netData, err := bufio.NewReader(c).ReadString('\n')
		if err != nil {
			connect=false
			disconnect_user(users, c.RemoteAddr().String(),c)
			break
		}

		temp := strings.TrimSpace(string(netData))
		request := strings.Split(temp, ":")
		option, err := strconv.Atoi(request[0]);

		if err != nil {
			log.Fatal(err)
			return
		}

		switch option {
		case AUTH:
			if (check_version(temp)){
				result = strconv.Itoa(check_user(temp, db, &id_client, &login))
				address_ip := c.RemoteAddr().String()
				if id_client != -1 {
					if test_connexion_open(users, address_ip) == false {
						if test_already_logged(users, login) == false {
							users.array_users[users.nb_users_connected].id = id_client
							users.array_users[users.nb_users_connected].ip_address_port = address_ip
							users.array_users[users.nb_users_connected].login = login
							users.nb_users_connected++
						} else {result = strconv.Itoa(AUTH_ALREADY_OPENED_SAME_LOGIN)}
					} else {result = strconv.Itoa(AUTH_ALREADY_OPENED_SAME_ADSRESS)}
				}
				for i := 0; i < users.nb_users_connected; i++ {
					fmt.Println(users.array_users[i].id, users.array_users[i].ip_address_port)
				}
			} else {
				result = strconv.Itoa(AUTH_WRONG_VERSION)
			}
			if (result == strconv.Itoa(AUTH_OK)) {
				fmt.Printf("connexion effectuée utilisateur : %s\n",request[1])
			} else {
				fmt.Printf("connexion refusée: code %d utilisateur %s\n", result, request[1])
			}
			//fmt.Printf("authentification , result : %d\n", result)
			print_users_connected(users)

    	case SIGN_UP:
			result = strconv.Itoa(create_profil(db, request, c))
			//fmt.Printf("sing up, result : %d\n", result)

		case CREATE_LOBBY:
			var result_local int
			*lobby=create_lobby(temp, lobby, nb_games, &result_local)
			result = strconv.Itoa(result_local)
			//fmt.Printf("creation lobby, result : %d\n", result)

    	case JOIN_LOBBY:
			result = print_lobby(lobby, nb_games)
			//fmt.Printf("join lobby, result : %d\n", result)

		case DISCONNECT:
			result = "Déconnexion effectuée\n"
			connect=false
			disconnect_user(users, c.RemoteAddr().String(),c)
			break

    default:
      fmt.Println("Commande inconnue : %d",option)
    }
		//fmt.Printf(temp+"\n")
		c.Write([]byte(fmt.Sprintf("%s:",string(result))))
		fmt.Println(result)
	}
	c.Close()
}

func main() {
	var lobby [NB_PARTIES]Partie
	var users users_connected
	users.nb_users_connected = 0;
	var nb_games = 0

	l :=  open_connexion()
	db := connexion_database()

	fmt.Println("En attente de connexion")

	for {
		c, err := l.Accept()
		if err != nil {
			log.Fatal(err)
			return
		}
		go handle_connection(c, db, &lobby, &nb_games, &users)
	}

  defer db.Close()
}
