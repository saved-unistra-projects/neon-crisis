extends Spatial

#### Attributs ####
onready var spawn_position = $SpawnPosition
	
func spawn(item_to_spawn):
	var instance = item_to_spawn.instance()
	instance.transform.origin = get_spawn_global_position()
	get_tree().current_scene.add_child(instance)
	
#### Getter ####
func get_spawn_global_position():
	return spawn_position.global_transform.origin
