extends "res://scenes/spawners/Spawner.gd"

#### Signaux ####
signal just_freed(spawner)

#### Attributs ####
onready var area = $SpawnPosition/Area
var bodies = 0

func move_player_to_spawn(player):
	player.transform.origin = spawn_position.global_transform.origin
	bodies += 1
	
func is_free():
	return bodies == 0

func _on_Area_body_shape_entered(_body_id, _body, _body_shape, _area_shape):
	var over_bodies = area.get_overlapping_bodies()
	bodies = over_bodies.size()

func _on_Area_body_shape_exited(_body_id, _body, _body_shape, _area_shape):
	var over_bodies = area.get_overlapping_bodies()
	bodies = over_bodies.size() - 1
	if bodies == 0:
		emit_signal("just_freed",self)
