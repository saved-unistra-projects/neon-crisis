extends "res://scenes/items/Item.gd"

#### Attributs ####
export var ammo_amount = 20

func pick_up(player):
	if player.has_method("add_ammo"):
		player.add_ammo(ammo_amount)
	queue_free()
