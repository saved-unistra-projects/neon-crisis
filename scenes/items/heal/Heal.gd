extends "res://scenes/items/Item.gd"

#### Attributs ####
export var heal_amount = 40

func pick_up(player):
	if player.has_method("set_health"):
		player.set_health(player.health + heal_amount)
	queue_free()
