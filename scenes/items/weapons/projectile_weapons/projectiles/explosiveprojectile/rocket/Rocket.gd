extends "res://scenes/items/weapons/projectile_weapons/projectiles/explosiveprojectile/ExplosiveProjectile.gd"

#### Attribut ####
export var damage = 40
export var knock_back_force = 10
onready var explosion_particles = $Explosion
onready var timer = $Explosion/Timer
onready var fire = $Fire
var exploded = false
var explosion_location

func process_explosion_hit(body):
	if body.has_method("take_hit"):
		body.take_hit(damage,player_info,false)
		var knock_back_vector = body.global_transform.origin - global_transform.origin
		knock_back_vector = knock_back_vector.normalized()
		if body.has_method("knock"):
			body.knock(knock_back_vector * knock_back_force)
	
func process_object_hit():
	if not exploded:
		$Model.hide()
		if timer.is_stopped():
			timer.start(explosion_particles.lifetime)
		explosion_particles.emitting = true
		fire.show()
		self.rotation_degrees = Vector3(0, 0, 0)
		explosion_location = fire.global_transform
		exploded = true
		
func _process(_delta):
	if exploded:
		fire.global_transform = explosion_location
		
