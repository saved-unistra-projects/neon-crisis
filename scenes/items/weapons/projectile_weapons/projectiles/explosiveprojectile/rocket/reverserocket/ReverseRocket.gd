extends "res://scenes/items/weapons/projectile_weapons/projectiles/explosiveprojectile/ExplosiveProjectile.gd"

#### Attribut ####
export var damage = 40
export var knock_back_force = 10
onready var explosion_particles = $Explosion
onready var timer = $Explosion/Timer
var exploded = false
var explosion_location
var old_transform

func process_explosion_hit(body):
	if body.has_method("take_hit"):
		body.take_hit(damage,player_info,false)
		var knock_back_vector = body.global_transform.origin - global_transform.origin
		knock_back_vector = knock_back_vector.normalized()
		if body.has_method("knock"):
			body.knock(knock_back_vector * knock_back_force)
	
func process_object_hit():
	if not exploded:
		$Effect.hide()
		if timer.is_stopped():
			timer.start(explosion_particles.lifetime)
		explosion_particles.emitting = true
		self.rotation_degrees = Vector3(0, 0, 0)
		exploded = true
		old_transform = self.global_transform
		
func _process(_delta):
	if exploded:
		self.global_transform = old_transform
		

		
