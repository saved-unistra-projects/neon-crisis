extends RigidBody

#warning-ignore-all:unused_argument
#warning-ignore-all:unused_class_variable

#### Attributs ####
export var projectile_force = 200
# Info du joueur qui tire ce projectile
var player_info setget set_player_info

func set_player_info(info):
	player_info = info
	
func _on_body_shape_entered(body_id, body, body_shape, local_shape):
	if body.is_in_group("Player"):
		player_hit(body_id, body, body_shape, local_shape)
	else:
		object_hit(body_id, body, body_shape, local_shape)
		
func player_hit(body_id,body,body_shape,local_shape):
	var shape = body.shape_owner_get_owner(body_shape)
	if shape.is_in_group("HeadPart"):
		player_head_hit(body_id, body, body_shape, local_shape)
	elif shape.is_in_group("BodyPart"):
		player_body_hit(body_id, body,body_shape, local_shape)
	
func player_head_hit(body_id, body, body_shape, local_shape):
	pass
	
func player_body_hit(body_id, body, body_shape, local_shape):
	pass

func object_hit(body_id, body, body_shape, local_shape):
	pass
