tool
extends "res://scenes/items/weapons/ray_cast_weapons/RayCastWeapon.gd"

#### Attributs ####
# On garde le joueur pour pouvoir faire un zoom
var player
var is_zooming = false
var is_equipped = false

func pick_up(_player):
	if NetworkManager.server:
		.pick_up(_player)
	player = _player

func _process(_delta):
	if player != null and player.equipped_weapon!=null and player.equipped_weapon.item_name=="Sniper":
		if Input.is_action_just_pressed("zoom"):
			if is_zooming:
				unzoom()
			else:
				zoom()

func drop():
	.drop()
	if player != null:
		player.unzoom()
		player = null

func zoom():
	if not is_zooming:
		is_zooming = true
		animation_player.play("zoom")

func unzoom():
	if is_zooming:
		is_zooming = false
		animation_player.play("unzoom")

func send_zoom():
	player.zoom()

func send_unzoom():
	player.unzoom()

func reload():
	.reload()
	if animation_player.current_animation == "reload":
		send_unzoom()
		is_zooming = false

# Underscores pour les warnings, et a, b, c, d pour respecter la signature du parent
func fire(_a, _b, _c):
	if animation_player.current_animation != "zoom" and animation_player.current_animation != "unzoom":
		.fire(_a, _b, _c)
		if is_zooming:
			animation_player.play("firezoom")
