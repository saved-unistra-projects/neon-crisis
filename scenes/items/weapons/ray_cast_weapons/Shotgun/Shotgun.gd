tool
extends "res://scenes/items/weapons/ray_cast_weapons/RayCastWeapon.gd"

#### Attributs ####
export var max_damage = 80
export var head_modifier = 20
var original_damage = body_damage

func process_fire(fire_point, fire_vector, info):
	var result = cast_ray(fire_point, fire_vector)
	if not result.empty():
		var dist = result.position.distance_to(self.global_transform.origin)
		var ratio = dist/weapon_max_range
		body_damage = int(1/ratio * original_damage)
		head_damage = body_damage + head_modifier
		.process_fire(fire_point, fire_vector, info)
