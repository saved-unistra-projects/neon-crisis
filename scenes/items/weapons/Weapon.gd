tool
extends "res://scenes/items/Item.gd"
#warning-ignore-all:unused_class_variable
#warning-ignore:return_value_discarded

#### Attributs ####
export var weapon_max_range = 1000
export var max_ammo : int = 20
export var max_magazine_size = 10
var ammo : int
var magazine_size : int
onready var hands = $Movables/Model/Hands

var is_reloading = false

# Raccourcis pour l'interface
var ammo_counter : Node
var weapon_list : Node

# Timer pour cooldown
onready var timer = $Timer

#### Outils editeur ####
export var preview_player : bool = false setget set_preview_player
var player_scene : PackedScene = preload("res://scenes/characters/player/Player.tscn")
var player_node
var camera_node

#### Setters ####
		
func set_preview_player(b):
	if Engine.editor_hint:
		preview_player = b
		if preview_player:
			player_node = player_scene.instance()
			add_child(player_node)
			player_node.name = "PreviewPlayer"
			player_node.owner = get_tree().edited_scene_root
			camera_node = player_node.get_node("Pivot/Camera").duplicate()
			add_child(camera_node)	
			camera_node.global_transform = player_node.get_node("Pivot/Camera").global_transform
			camera_node.name = "PreviewCamera"
			camera_node.owner = get_tree().edited_scene_root
		else:
			if player_node != null:
				if camera_node != null:
					camera_node.queue_free()
					camera_node = null
				player_node.queue_free()
				
				player_node = null
	
func _ready():
	ammo = max_ammo
	magazine_size = max_magazine_size
	

func set_ammo_counter():
	ammo_counter = GUI.hud.get_element("AmmoCounter")
	weapon_list = GUI.hud.get_element("WeaponList")
	weapon_list.add_weapon(item_name, magazine_size, ammo)
	
func moving():
	animation_player.play("move")
	
func stop_moving():
	if animation_player.current_animation == "move":
		animation_player.play("default")
	
	
func pick_up(_player):
	.pick_up(_player)
	
	var weap = _player.get_weapon(item_name)
	
	if(weap != null):
		weap.add_ammo(ammo)
		queue_free()
		return


		
func switch_to():
	if ammo_counter != null:
		ammo_counter.weapon_name = item_name
	if weapon_list != null:
		weapon_list.equip(item_name)
	update_gui()
	hands = $Movables/Model/Hands
	if hands != null:
		hands.show()
	

func fire(_fire_point, _fire_vector,_info):
	# On ne tire que si le cooldown est terminé
	if timer.is_stopped() and (magazine_size > 0 or max_ammo == 0):
		magazine_size -= 1
		if animation_player.current_animation == "fire":
			animation_player.stop()
		animation_player.play("fire")
		process_fire(_fire_point,_fire_vector,_info)
		update_gui()
		timer.start()
		
func update_gui():
	if ammo_counter != null:
		ammo_counter.current_ammo = magazine_size
		if max_ammo == 0:
			ammo_counter.max_ammo = -1
		else:
			ammo_counter.max_ammo = ammo
	if weapon_list != null:
		if max_ammo == 0:
			weapon_list.update_info(item_name, magazine_size, -1)
		else:
			weapon_list.update_info(item_name, magazine_size, ammo)

func reload():
	if ammo>0 and animation_player.current_animation == "":
		animation_player.play("reload")
		
func effectively_reload(animation_finished):
	if animation_finished == "reload":
		var to_add = max_magazine_size - magazine_size
		if ammo >= to_add:
			ammo -= to_add
			magazine_size += to_add
		else:
			ammo = 0
			magazine_size += ammo
		update_gui()

func add_ammo(amount: int ):
	ammo += amount
	#warning-ignore:narrowing_conversion
	ammo = clamp(ammo, 0, max_ammo)
	update_gui()
		
# Fonction qui indique que faire lorsque que l'on tire
func process_fire(_fire_point,_fire_vector,_info):
	# Fonction "virtuelle"
	pass
		
	
func cast_ray(point, normal) -> Dictionary:
		# Récupération du point d'origine
		var from = point
		# Récupération du point d'arrivée (avec le vecteur donné)
		var to = from + normal * weapon_max_range
		var space_state = get_world().direct_space_state
		# Récupération de la collision
		return space_state.intersect_ray(from,to)
		
func drop():
	print("dropped")
	.drop()
	stop_moving()
	if ammo_counter != null:
		ammo_counter.clear()
	if weapon_list != null:
		weapon_list.remove(item_name)
	if hands != null:
		hands.hide()
		
func set_ammo_and_mag(new_ammo, new_mag):
	ammo = new_ammo
	magazine_size = new_mag
	
