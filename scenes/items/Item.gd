extends Spatial

#### Constantes ####
const RAY_LENGTH = 1000

#### Attributs ####
onready var pickup_area = $Movables/PickupArea
# L'item est sur le sol
export var is_on_ground = true setget set_is_on_ground
export var is_pickable = true setget set_is_pickable
export var pickable_delay = 1
# Lumière lorsque l'arme est au sol
onready var spotlight = $Movables/SpotLight
onready var animation_player = $AnimationPlayer
export var item_name : String

#### Setters ####

# Indique si l'objet est sur le sol ou non (cela a un effet sur le spotlight
# ainsi que l'animation lorsque l'objet est au sol)
func set_is_on_ground(b):
	is_on_ground = b
	if spotlight != null:
		spotlight.visible = is_on_ground
	if animation_player != null:
		if is_on_ground:
			animation_player.play("on_ground")
		else:
			animation_player.play("default")

func set_is_pickable(b):
	is_pickable = b
	if pickup_area != null:
		pickup_area.monitorable = b
			
func _ready():
	if is_on_ground and not Engine.editor_hint:
		animation_player.play("on_ground")
		drop()
	else:
		animation_player.play("default")
	set_is_on_ground(is_on_ground)
			
func pick_up(_player):
	# Fonction virtuelle
	pass
	
func drop():
	var global_translation = global_transform.origin
	var direct_space = get_world().direct_space_state
	var result = direct_space.intersect_ray(global_translation,
	Vector3(global_translation.x,-RAY_LENGTH,global_translation.z), get_tree().get_nodes_in_group("Player"))
	var target = translation
	if not result.empty():
		target.y = result.position.y
	var tween = Tween.new()
	add_child(tween)
	var time = abs(sqrt(2*(translation.y-target.y)/ProjectSettings.get_setting("physics/3d/default_gravity")))
	tween.interpolate_property(self,"translation",translation,target,time,Tween.TRANS_QUAD,Tween.EASE_IN)
	tween.connect("tween_all_completed",tween,"queue_free")
	tween.start()
	set_is_on_ground(true)
	var drop_timer = Timer.new()
	add_child(drop_timer)
	drop_timer.wait_time = time + pickable_delay
	drop_timer.one_shot = true
	drop_timer.start()
	drop_timer.connect("timeout",self,"finished_dropping",[drop_timer],CONNECT_ONESHOT)
	
func finished_dropping(timer):
	timer.queue_free()
	set_is_pickable(true)

