extends Node

const WORLD_SCENE = "res://scenes/levels/testscene/World.tscn"

var map

func _get_configuration_warning():
	if get_parent() == null:
		return "This node must have a map as its only child"
	else:
		map = get_parent()
		return ""

func _ready():
	get_tree().get_root().get_node("MenuHolder").queue_free()
	# Initialisation de la carte
	map = load(WORLD_SCENE).instance()
	add_child(map)
	# get_tree().set_current_scene(map) marche pas trop jsp pq :(
