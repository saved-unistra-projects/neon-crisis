extends Control

#### Signaux ####
signal clearing_menu()

#### Attributs ####
# La scène avant le changement de menu. Utile pour les boutons de retour
var menu_stack = []
var current_menu : PackedScene setget set_current_menu
var current_menu_instance : Node
export var first_menu : PackedScene
export var start_empty = false
export var background_scene : PackedScene
onready var placeholder = $PlacerHolder

#### Setters ####
	
func set_current_menu(menu : PackedScene):
	if menu != null:
		clear_menu()
		current_menu = menu
		current_menu_instance = current_menu.instance()
		placeholder.add_child(current_menu_instance)
		current_menu_instance.propagate_call("set_holder",[self])
	
func _ready():
	if not start_empty:
		set_current_menu(first_menu)
		if background_scene != null:
			call_deferred("add_child",background_scene.instance())

func change_menu(menu : PackedScene):
	if current_menu != null:
		menu_stack.append(current_menu)
	set_current_menu(menu)

func go_back():
	if not menu_stack.empty():
		set_current_menu(menu_stack.pop_back())
		
func restore_first_menu():
	menu_stack.clear()
	set_current_menu(first_menu)
	
func clear_menu():
	if current_menu_instance != null:
		current_menu_instance.queue_free()
		current_menu_instance = null
	emit_signal("clearing_menu")
