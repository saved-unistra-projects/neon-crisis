extends "res://scenes/gui/menus/Menu.gd"

func _ready():
	propagate_call("update_setting",[true])
	
func startup_init():
	hide()
	propagate_call("update_setting",[true])
	propagate_call("apply_setting")
	queue_free()

func _on_Apply_pressed():
	propagate_call("apply_setting")
	UserSettings.save_settings()


func _on_Reset_pressed():
	propagate_call("reset_setting")

func _on_Restore_pressed():
	propagate_call("remove_setting")
	propagate_call("update_setting")
	
