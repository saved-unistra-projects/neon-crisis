extends "res://scenes/gui/menus/Menu.gd"

#### Attributs ####

func set_holder(h):
	.set_holder(h)
	if holder != null:
		holder.connect("clearing_menu",self,"closing")

func _ready():
	
	if GUI.out_of_game:
		GUI.show()
	GUI.hud.show()

	
	GUI.hud.propagate_call("set_draggable",[true])
	GUI.hud.propagate_call("update_setting")
	
func _on_Apply_pressed():
	GUI.hud.propagate_call("apply_setting")
	GUI.hud.propagate_call("update_setting")
	UserSettings.save_settings()

func _on_Restore_pressed():
	GUI.hud.propagate_call("remove_setting")
	GUI.hud.propagate_call("update_setting")

func _on_Reset_pressed():
	GUI.hud.propagate_call("reset_setting")

func closing():
	if GUI.out_of_game:
		GUI.hide()
	GUI.hud.hide()
	GUI.hud.propagate_call("set_draggable", [false])
