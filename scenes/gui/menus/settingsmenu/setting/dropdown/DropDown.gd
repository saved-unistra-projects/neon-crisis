extends "res://scenes/gui/menus/settingsmenu/setting/Setting.gd"

#### Attributs ####
export(Array, String) var options
export (Array, String) var option_values
onready var drop_down = $HBoxContainer/Options
var default_value

func _ready():
	label_path = "HBoxContainer/Label"
	set_text(text)
	var i = 0
	for option in options:
		drop_down.add_item(option,i)
		i += 1
	if not option_values.empty():
		default_value = option_values[0]
		
func _on_Options_item_selected(id):
	set_setting_value(option_values[id])
	
func set_setting_value(val):
	.set_setting_value(val)
	if val != null:
		var index = option_values.find(val)
		if index!= -1:
			drop_down.selected = index
