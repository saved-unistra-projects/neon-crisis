extends "res://scenes/gui/menus/settingsmenu/setting/dropdown/DropDown.gd"

#### Attributs ####
var current_val : String

func set_setting_value(val):
	.set_setting_value(val)
	if val != null:
		current_val = val
	else:
		current_val = "en"
		
func apply_setting():
	if current_val != null:
		TranslationServer.set_locale(current_val)
	.apply_setting()
