tool
extends "res://scenes/gui/menus/settingsmenu/setting/Setting.gd"

#### Attributs ####
export var default_value = false
export var disabled = false

onready var toggle = $HBoxContainer/CheckButton

func _ready():
	label_path = "HBoxContainer/Label"
	set_text(text)
	toggle.disabled = disabled
	
func set_setting_value(val):
	.set_setting_value(val)
	if toggle != null and setting_value != null:
		toggle.pressed = setting_value


func _on_CheckButton_toggled(_button_pressed):
	set_setting_value(toggle.pressed)
