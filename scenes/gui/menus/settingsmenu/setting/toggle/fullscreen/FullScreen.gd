extends "res://scenes/gui/menus/settingsmenu/setting/toggle/Toggle.gd"

func apply_setting():
	.apply_setting()
	if setting_value != null:
		OS.window_fullscreen = setting_value
	else:
		OS.window_fullscreen = default_value
