tool
extends "res://scenes/gui/menus/settingsmenu/setting/Setting.gd"

#### Attributs ####
var action_string : String
onready var key_button = $HBoxContainer/Button
var change_key_message = "Press a Key"
var can_change : bool = false
var default_value

func _ready():
	var substrings = setting_path.split("/")
	action_string = substrings[substrings.size()-1]
	label_path = "HBoxContainer/Label"
	set_text(text)
	InputMap.load_from_globals()
	if InputMap.get_action_list(action_string).size() > 0:
		default_value = InputMap.get_action_list(action_string)[0]
	
func set_setting_value(val):
	if val != null:
		.set_setting_value(val)
	elif default_value != null:
		set_setting_value(default_value)
	if setting_value != null:
		key_button.text = setting_value.as_text()
		if setting_value is InputEventMouseButton:
			key_button.text = "Mouse Button " + String(setting_value.button_index)
		
func apply_setting():
	.apply_setting()
	if setting_value != null:
		if not InputMap.get_action_list(action_string).empty():
			InputMap.action_erase_event(action_string, InputMap.get_action_list(action_string)[0])
			
		for i in InputMap.get_actions():
			if InputMap.action_has_event(i, setting_value):
				InputMap.action_erase_event(i, setting_value)
		if not InputMap.has_action(action_string):
			InputMap.add_action(action_string)
		InputMap.action_add_event(action_string,setting_value)
	


func _on_Button_toggled(button_pressed):
	if button_pressed:
		key_button.text = change_key_message
		can_change = true
		
func _input(event):
	if can_change:
		if event is InputEventKey or event is InputEventMouseButton:
			if event is InputEventMouseButton or event.scancode != KEY_ESCAPE:
				set_setting_value(event)
			else:
				set_setting_value(setting_value)
			key_button.pressed = false
			can_change = false
