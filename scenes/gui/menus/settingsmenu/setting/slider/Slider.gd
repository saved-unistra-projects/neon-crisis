tool
extends "res://scenes/gui/menus/settingsmenu/setting/Setting.gd"

#### Attributs ####
export var disabled = false
export var min_value = 0 setget set_min_value
export var max_value = 100 setget set_max_value
export var default_value = 40 setget set_default_value
export var step :float = 0 setget set_step
export var rounded : bool = true setget set_rounded
export var multiplicator : float = 1
onready var slider = $HBoxContainer/Slider
onready var value_display = $HBoxContainer/ValueDisplay

func _ready():
	set_min_value(min_value)
	set_max_value(max_value)
	set_setting_value(default_value)
	set_rounded(rounded)
	set_step(step)
	label_path = "HBoxContainer/Label"
	set_text(text)
	slider.editable = !disabled

#### Setters ####

func set_rounded(b):
	rounded = b
	if slider != null:
		slider.rounded = rounded
func set_step(s):
	step = s
	if slider != null:
		slider.step = step

func set_min_value(val):
	min_value = val
	if slider != null and min_value != null:
		slider.min_value = min_value
	
func set_max_value(val):
	max_value = val
	if slider != null and max_value != null:
		slider.max_value = max_value
	
func set_setting_value(val):
	if val == null and default_value != null:
		set_setting_value(default_value)
		return
	.set_setting_value(val)
	if value_display != null and val != null:
		value_display.text = String(val)
	if slider != null and setting_value != null:
		slider.value = setting_value
		
func set_default_value(val):
	default_value = val
	set_setting_value(val)
	

func _on_Slider_value_changed(value):
	set_setting_value(value)
