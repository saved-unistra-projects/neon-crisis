tool
extends Control

#### Attributs ####
onready var label = null
var label_path = null
export var text : String setget set_text
export var setting_path : String 
var setting_value setget set_setting_value
var initial_value
var old_value

#### Setters ####
func set_setting_value(val):
	if val != null:
		setting_value = val

func set_text(t):
	text = t
	if label_path != null:
		label = get_node(label_path)
	if label != null:
		label.text = text
		
func update_setting(first_update = false):
	var val = UserSettings.get_setting(setting_path)
	set_setting_value(val)
	initial_value = val
	if first_update:
		old_value = setting_value
	
func reset_setting():
	set_setting_value(old_value)
	
	
func apply_setting():
		UserSettings.set_setting(setting_path,setting_value)
		old_value = setting_value
		
func remove_setting():
	UserSettings.remove_setting(setting_path)
	
func _exit_tree():
	if old_value != null:
		set_setting_value(old_value)
		apply_setting()
		
