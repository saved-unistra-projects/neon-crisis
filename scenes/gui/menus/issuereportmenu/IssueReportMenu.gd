extends "res://scenes/gui/menus/Menu.gd"

#### Constantes ####
const PATH = "https://neoncrisis.pleuh.fr/web/lib/controllers/sentIssue.php"

#### Strings ####
var error_message = tr("An error occured, try again later.")
var success_message = tr("Your issue was sent!")

#### Attributs ####
onready var title = $MarginContainer/HBoxContainer/VBoxContainer/Panel/MarginContainer/VBoxContainer/IssueTitle
onready var description = $MarginContainer/HBoxContainer/VBoxContainer/Panel/MarginContainer/VBoxContainer/IssueDescription
onready var steps = $MarginContainer/HBoxContainer/VBoxContainer/Panel/MarginContainer/VBoxContainer/StepsToReproduce
onready var type_options = $MarginContainer/HBoxContainer/VBoxContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer2/Type
onready var http = $HTTPRequest
onready var label = $MarginContainer/HBoxContainer/VBoxContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer2/Label

var type = ["Bug","Feature"] 


func _on_Submit_pressed():
	if title.text != "" and description.text != null:
		send_issue()
		
func send_issue():
	var full_text = format_issue()
	var data =  'subject=' + title.text.http_escape()  + '&detail='  + full_text.http_escape()  + '&tag=' +  "User Issue,".http_escape() + type[type_options.selected].http_escape()
	var headers = []
	
	print(PATH + data)
	
	http.request(PATH + "?" + data, headers,true,HTTPClient.METHOD_GET)
 
func format_issue():
	if steps.text != "":
		return description.text + "\n\n**Steps to reproduce :**\n\n" + steps.text
	else:
		return description.text


func _on_HTTPRequest_request_completed(_result, _response_code, _headers, _body : PoolByteArray):
	if _response_code == 0:
		print_success()
	else:
		print_error()
	
func print_error():
	label.text = error_message
	
func print_success():
	label.text = success_message
	title.text = ""
	description.text = ""
	steps.text = ""
