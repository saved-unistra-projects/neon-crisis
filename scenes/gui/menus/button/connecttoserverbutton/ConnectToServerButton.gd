extends "res://scenes/gui/menus/button/Button.gd"

export var local = false

func _on_Button_pressed():
	var ip_addr = "127.0.0.1"
	if not local:	
		ip_addr = "91.121.81.74"#"91.121.81.74"
	var port = "5647"
	var player_name = "Joueur"
	
	# Init du joueur local
	# Dans un futur proche, on devra EN PLUS récuperer les infos d'authent:
	# => login(=pseudo) + bdd_id
	NetworkManager.player_info.name = player_name
	NetworkManager.join_server(ip_addr, int(port))
	GUI.enable()
