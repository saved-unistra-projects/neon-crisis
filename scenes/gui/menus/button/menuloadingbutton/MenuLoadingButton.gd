extends "res://scenes/gui/menus/button/Button.gd"

#### Attributs ####

export var destination_scene : PackedScene

func _on_Button_pressed():
	if holder != null:
		holder.change_menu(destination_scene)
