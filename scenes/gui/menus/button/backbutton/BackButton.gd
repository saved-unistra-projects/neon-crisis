extends "res://scenes/gui/menus/button/Button.gd"

#warning-ignore-all:return_value_discarded

#### Attributs #####
export var highlight_color = Color.red

func _on_BackButton_pressed():
	if holder!=null:
		holder.go_back()
		
func _on_Button_mouse_entered():
	tween.stop_all()
	tween.interpolate_property(self,"self_modulate",Color.white,highlight_color,hover_animation_time,Tween.TRANS_LINEAR,Tween.EASE_IN)
	tween.start()
	
func _on_Button_mouse_exited():
	tween.stop_all()
	tween.interpolate_property(self,"self_modulate",self_modulate,Color.white,hover_animation_time,Tween.TRANS_LINEAR,Tween.EASE_IN)
	tween.start()
