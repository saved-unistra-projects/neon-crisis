extends Button

#warning-ignore-all:return_value_discarded

#### Attributs ####
onready var expand_bar : StyleBoxFlat = get_stylebox("hover","")
onready var tween : Tween = $Tween
export var hover_animation_time : float = 0.1
# Le menu holder
var holder : Node = null

#### Setters ####
func set_holder(new_holder : Node):
	holder = new_holder

func _ready():
	pass

func _on_Button_mouse_entered():
	var size = rect_size.x
	tween.interpolate_property(expand_bar,"border_width_left",size/2,0,hover_animation_time,Tween.TRANS_LINEAR,Tween.EASE_OUT)
	tween.interpolate_property(expand_bar,"border_width_right",size/2,0,hover_animation_time,Tween.TRANS_LINEAR,Tween.EASE_OUT)
	tween.start()


func _on_Button_mouse_exited():
	tween.reset_all()
	tween.stop_all()
