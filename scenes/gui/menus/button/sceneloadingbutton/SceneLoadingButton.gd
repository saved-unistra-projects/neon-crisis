extends "res://scenes/gui/menus/button/Button.gd"

#warning-ignore-all:return_value_discarded

#### Attributs ####
export var target_scene : PackedScene
export var enable_gui : bool = true
export var disable_gui : bool = false



func _on_Button_pressed():
	if enable_gui:
		GUI.enable()
	elif disable_gui:
		GUI.disable()
	get_tree().change_scene_to(target_scene)

