extends "res://scenes/gui/menus/Menu.gd"

#### Attribut ####
onready var login_text_box = $Panel/HBoxContainer/VBoxContainer/Panel/MarginContainer/VBoxContainer/Login
onready var password_text_box = $Panel/HBoxContainer/VBoxContainer/Panel/MarginContainer/VBoxContainer/Password
onready var result_label = $Panel/HBoxContainer/ResultLabel

func _ready():
	pass


func _on_Connect_pressed():
	NetworkServerAccess.send_auth(login_text_box.text, password_text_box.text,NetworkServerAccess.AUTH)
	NetworkServerAccess.connect("new_received_message",self,"on_server_response",[],CONNECT_ONESHOT)
	
func on_server_response(message):
	match int(message["flag"]):
		NetworkServerAccess.AUTH_OK:
			result_label.text = tr("You're connected!")
			var timer = Timer.new()
			add_child(timer)
			timer.start(2)
			timer.connect("timeout",self,"to_main_menu")
		NetworkServerAccess.AUTH_WRONG_LOGIN:
			result_label.text = tr("Unknown account")
		NetworkServerAccess.AUTH_WRONG_PWD:
			result_label.text = tr("Wrong password")
		NetworkServerAccess.AUTH_WRONG_VERSION:
			result_label.text = tr("Sorry, this version is not supported anymore.\nPlease update the game")
		NetworkServerAccess.AUTH_ALREADY_OPENED:
			result_label.text = tr("This account is already in use")
			
func to_main_menu():
	holder.change_menu(preload("res://scenes/gui/menus/mainmenu/MainMenu.tscn"))
			
			
			
	
