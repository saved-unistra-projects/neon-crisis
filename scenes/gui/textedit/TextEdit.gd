tool
extends TextEdit

#### Attributs ####
export var placeholder : String
export(float, 0, 1) var alpha = .6
onready var label = $MarginContainer/Label

func _ready():
	label.text = placeholder
	label.self_modulate = Color(1,1,1,alpha)

func _on_TextEdit_text_changed():
	if text == "":
		label.show()
	else:
		label.hide()
		
