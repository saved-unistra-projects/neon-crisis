extends CPUParticles

#warning-ignore:return_value_discarded

var timer : Timer

func _ready():
	timer = $Timer
	timer.start(lifetime)
	timer.connect("timeout",self,"queue_free")
