extends KinematicBody

#### Constantes ####
# Vitesse de saut
const JUMP_SPEED = 20
# Pente max de ce que l'on condière un "escalier" (pour exclure les murs)
const STAIRS_MAX_SLOPE = 1
# Decceleration du knock
const KNOCK_DEACCEL = 3

#### Signaux ####
signal dead(info)

#### Attributs ####
# Points de vie
var health = 100 setget set_health

# Vitesse
export var speed = 15

# Infos joueur à passer lors des tirs
var player_info = {} setget set_player_info
# Infos du dernier coup reçu
var last_hit_info = {}

# Direction et velocité
var direction = Vector3.ZERO
var velocity = Vector3.ZERO

# Permet de récupérer le vecteur de la dernière collision
var last_collision_vector : Vector3

# Sensibilité de la souris
var mouse_sensitivity : float

# Caméra, pivot, et référence pour l'arme
onready var camera = $Pivot/Camera
onready var pivot = $Pivot
onready var weapon_reference = $Pivot/WeaponReference
onready var tween = $Tween

# Le joueur est ou non le joueur dont les inputs influent
#export var is_main_player = true setget set_is_main_player

# Particules de sang
onready var blood = preload("../particles/Blood.tscn")

# Raycast pour les escaliers
onready var stair_ray_casts = $StairRayCasts
onready var ray_cast_checker = $RayCastChecker
# Vecteur de knock
var knock_vector : Vector3 = Vector3(0,0,0)
var old_knock_vector : Vector3 = Vector3(0,0,0)

#### Armes ####
# Tableau de NodePath représentant l'inventaire
export var inventory = []
var equipped_weapon = null
var equipped_weapon_index = -1 #setget equip_weapon

const TREE_WROLD = "NetworkManager/TeamDeathmatchManager/World"

func set_player_info(info):
	player_info = info

#### Getters ####
func get_weapon_reference_position():
	return weapon_reference.translation

# Prend un coup. Si blood_bool est vrai, alors du sang sera affiché à la position
# hit_position
func take_hit(damage, info, blood_bool=true, hit_position=Vector3(0,0,0)):
	last_hit_info = info
	# Changer la vie ...
	# Si le tir vient de mon équipe, je ne prend pas de dégâts
	if info!=null and info["team"] == player_info["team"]:
		# Sauf si il vient de moi, je prend les moitié des dégâts
		if info["username"] == player_info["username"]:
			set_health(health - damage/2)
	else:
		set_health(health - damage)
	# Puis afficher des particules de sang à l'endroit de l'impact
	if blood_bool:
		var blood_instanced : CPUParticles = blood.instance()
		# On l'ajout à la scene global pour pas que le sang "suive" le joueur
		#get_tree().current_scene.add_child(blood_instanced)
		get_tree().get_root().get_node(TREE_WROLD).add_child(blood_instanced)
		blood_instanced.translation = hit_position
		# Je précise que les particules doivent emettre, parce que sinon ça fait n'imp...
		blood_instanced.emitting = true
	
func knock(new_knock_vector : Vector3):
	knock_vector += new_knock_vector

# Ajoute des munitions de montant ammo à toutes les armes de l'inventaire
func add_ammo(ammo):
	for weapon in inventory:
		if weapon!=null and weapon.has_method("add_ammo"):
			weapon.add_ammo(ammo)

remote func set_health(new_health):
	health = new_health
	#if health <= 0:
	#	die(last_hit_info)

remote func update_trans_rot(pos, rot, head_rot):
	self.show()
	#add collision
	#print(self.collision_mask)
	#set_collision_mask_bit(2,true)
	#set_collision_mask_bit(0,false)
	translation = pos
	rotation = rot
	camera.rotation = head_rot

# Fonction pour changer le vecteur de knock, c'est à dire le vecteur de "recul"
# lorsque l'on prend une explosion
remote func fire(_fire_point, _fire_vector, _info):
	var tmp_equipped_weapon = get_node(str(_info.spawn_path) + "/" + str(_info.net_id)).equipped_weapon
	if tmp_equipped_weapon != null:
		tmp_equipped_weapon.fire(_fire_point, _fire_vector, _info, 1)

remote func set_equipped_weapon(path):
	if has_node(path[0]):
		equipped_weapon = get_tree().get_root().get_node(path[0])
	else:
		equipped_weapon = null

# Fonction qui tue le personnage. Pour l'instant, cache simplement le modèle
remote func die(info):
	self.hide()
	#emit_signal("dead",info)
	
remote func rebirth():
	self.show()
	health = 100

remote func reload():
	if equipped_weapon != null:
		if equipped_weapon.has_method("reload"):
			equipped_weapon.reload()

remote func hide_player():
	self.hide()
	#remove collision
	
remote func drop(weapon_index):
	var weapon = equipped_weapon
	#var weapon = inventory[weapon_index]
	var position = weapon.global_transform.origin
	pivot.remove_child(weapon)
	#get_tree().current_scene.add_child(weapon)
	get_tree().get_root().get_node(TREE_WROLD).add_child(weapon)
	weapon.global_transform.origin = position
	weapon.global_transform.origin.x += 10
	#inventory.remove(weapon_index)
	equipped_weapon_index = -1
	if weapon.has_method("drop"):
		weapon.drop()
	equipped_weapon = null

remote func add_weapon_to_inventory_with_path(weapon_path):
	var weapon = get_tree().get_root().get_node(weapon_path)
	print('add_to_inv')
	pivot.add_child(weapon)
	weapon.hide()
	weapon.global_transform = weapon_reference.global_transform
	inventory.append(weapon)
