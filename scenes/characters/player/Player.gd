extends KinematicBody

#### Constantes ####
# Vitesse de saut
const JUMP_SPEED = 20
# Pente max de ce que l'on condière un "escalier" (pour exclure les murs)
const STAIRS_MAX_SLOPE = 1
# Decceleration du knock
const KNOCK_DEACCEL = 3
# Snap vector par défault
const DEFAULT_SNAP_VECTOR = Vector3(0,-3,0)

#### Attributs ####
# Points de vie
var health = 100 setget set_health
var max_health = 100

# En train de sauter ?
var jumping = false

# Vitesse
export var speed = 15

# Infos joueur à passer lors des tirs
var player_info = {} setget set_player_info
# Infos du dernier coup reçu
var last_hit_info = {}

# Direction et velocité
var direction = Vector3.ZERO
var velocity = Vector3.ZERO

# Permet de récupérer le vecteur de la dernière collision
var last_collision_vector : Vector3

# Sensibilité de la souris
var mouse_sensitivity : float

# Caméra, pivot, et référence pour l'arme
onready var camera = $Pivot/Camera
onready var pivot = $Pivot
onready var weapon_reference = $Pivot/WeaponReference
onready var tween = $Tween

# Le joueur est ou non le joueur dont les inputs influent
export var is_main_player = true setget set_is_main_player

# Particules de sang
onready var blood = preload("particles/Blood.tscn")

# Raycast pour les escaliers
onready var stair_ray_casts = $StairRayCasts
onready var ray_cast_checker = $RayCastChecker
# Vecteur de knock
var knock_vector : Vector3 = Vector3(0,0,0)
var old_knock_vector : Vector3 = Vector3(0,0,0)

#### Armes ####
# Tableau de Nodes représentant l'inventaire
var inventory = []
var equipped_weapon = null
var equipped_weapon_index = -1 #setget equip_weapon

const TREE_WROLD = "NetworkManager/TeamDeathmatchManager/World"

var death_count = 0
#### Setters ####
func set_is_main_player(b):
	set_process_input(b)
	camera.current = b
	is_main_player = b

remote func set_health(new_health):
	health = clamp(new_health,0,max_health)
	GUI.hud.get_element("HealthBar").set_health(health)

func set_player_info(info):
	player_info = info

#### Getters ####
func get_weapon_reference_position():
	return weapon_reference.translation
	
########## Initialisations ##########

# Fonction ready exécutée à l'entrée dans l'arbre
func _ready():
	init_settings()
	# Différenciation du joueur et du dummy (test principalement)
	set_is_main_player(is_main_player)
	camera.current = is_main_player
	set_health(health)

func  init_settings(setting_path = ""):
	if setting_path == "":
		UserSettings.connect("setting_changed",self,"init_settings")
		
	if setting_path== "" or setting_path == "game/mouse/sensitivity":
		if UserSettings.get_setting("game/mouse/sensitivity") != null:
			mouse_sensitivity = UserSettings.get_setting("game/mouse/sensitivity")/float(100)
		else:
			mouse_sensitivity = 0.05
	if setting_path == "" or setting_path == "video/camera/fov":
		if UserSettings.get_setting("video/camera/fov") != null:
			camera.fov = UserSettings.get_setting("video/camera/fov")
		else:
			camera.fov = 70
			
########## Process et mouvements ##########
		
# Fonction exécutée à chaque frame lors de l'update de la physique
func _physics_process(delta):
	if is_network_master() and is_main_player and not GUI.menu_displayed:
		process_input(delta)
	if GUI.menu_displayed:
		direction = Vector3.ZERO
	process_movement(delta)

# Fonction de process des touches entrées par le joueur
func process_input(_delta):
	# Process des inputs de mouvement
	# Chaque input est convertit. Si le joueur appuie sur haut, alors on augmente
	# la valeur y du vecteur, etc...
	# Enfin, on convertit ce vecteur 2D en vecteur dans le monde 3D, projeté
	# sur les axes x et z du joueur.
	var input_movement = Vector2(0,0)
	if Input.is_action_pressed("ui_up"):
		input_movement.y += 1
	if Input.is_action_pressed("ui_down"):
		input_movement.y -= 1
	if Input.is_action_pressed("ui_left"):
		input_movement.x += 1
	if Input.is_action_pressed("ui_right"):
		input_movement.x -= 1
	input_movement = input_movement.normalized()
	direction = Vector3.ZERO
	direction += get_global_transform().basis.z * input_movement.y
	direction += get_global_transform().basis.x * input_movement.x
	direction = direction.normalized()
	# Process du saut
	if Input.is_action_just_pressed("jump"):
		jump()
	# Process de la touche shift
	if Input.is_action_pressed("sneaky"):
		speed = 5
	# Process de l'arrêt de la touche shift
	if Input.is_action_just_released("sneaky"):
		speed = 10
	# Process du clic pour tirer
	if Input.is_action_pressed("fire"):
		# On vérifie qu'on porte bien une arme avant d'appeler sa fonction fire,
		# avec comme argument le centre de l'écran (on indique à l'arme à partir
		# d'où on peut tirer un rayon)
		if equipped_weapon != null:
			equipped_weapon.fire(camera.project_ray_origin(get_viewport().size/2),camera.project_ray_normal(get_viewport().size/2),player_info,0)
			rpc_id(1, "fire", camera.project_ray_origin(get_viewport().size/2),camera.project_ray_normal(get_viewport().size/2),player_info)
	# Process de la touche pour recharger
	if Input.is_action_just_pressed("reload"):
		if equipped_weapon != null:
			if equipped_weapon.has_method("reload"):
				equipped_weapon.reload()
				rpc("reload")
	# Process de la touche pour lâcher l'arme
	if Input.is_action_just_pressed("drop"):
		if equipped_weapon != null:
			drop(equipped_weapon_index)

func process_movement(delta):
	# Process le déplacement
	# Application de la gravité
	velocity += delta * ProjectSettings.get_setting("physics/3d/default_gravity") * ProjectSettings.get_setting("physics/3d/default_gravity_vector")
	# Application des mouvements
	velocity.x = (direction * speed).x
	velocity.z = (direction * speed).z

	# Code utilisé pour appliquer une force de repousse (fonction knock)
	# Ce code se trouve ici car il faut appliquer la force à chaque frame en
	# la réduisant à chaque fois.
	if knock_vector != Vector3.ZERO:
		velocity.y -= old_knock_vector.y
		velocity += knock_vector
		# old_knock vector est utilisé car la composante y n'est pas reset à chaque
		# tour comme x et z.
		old_knock_vector = knock_vector
		knock_vector -= knock_vector* KNOCK_DEACCEL * delta
		if knock_vector.length() < 1:
			knock_vector = Vector3.ZERO
			old_knock_vector = Vector3.ZERO
	# Vérification de la présence d'escaliers. Voir la fonction
	check_for_stairs(delta)
	# Application effective de la vélocité
	var snap_vector = DEFAULT_SNAP_VECTOR
	if jumping:
		snap_vector = Vector3.ZERO
		jumping = false
		
	velocity = move_and_slide_with_snap(velocity, snap_vector,Vector3(0,1,0))
	#velocity = move_and_slide(velocity, Vector3(0,1,0))
	rpc_unreliable_id(1, "update_trans_rot", translation, rotation, get_node("Pivot/Camera").rotation)

remote func update_trans_rot(pos, rot, head_rot):
	translation = pos
	rotation = rot
	camera.rotation = head_rot
	
# Fonction de vérification de la présence d'escaliers
func check_for_stairs(_delta):
	# On vérifie que le joueur a input un mouvement et est sur le sol
	if direction.length()>0 and is_on_floor():
		# Pour chaque raycast autour du joueur (il y en a 8)
		for stair_ray_cast in stair_ray_casts.get_children():
			# Si un des raycast croise quelque chose, alors c'est potentiellement un escalier !
			if stair_ray_cast.is_colliding():
				# On récupère la normale de l'escalier...
				var collision_normal = stair_ray_cast.get_collision_normal()
				# ... pour calculer l'angle de sa surface...
				var stairs_angle = rad2deg(acos(collision_normal.dot(Vector3(0,1,0))))
				# ... et le comparer à l'angle maximum acceptable
				if stairs_angle < STAIRS_MAX_SLOPE:
					var collision_point = stair_ray_cast.get_collision_point()
					var collision_point_y = collision_point.y
					var ground_y = to_global(stair_ray_casts.translation).y + stair_ray_cast.cast_to.y
					collision_point.y = 0
					# Vérifie que le joueur va bien dans la direction de l'escalier
					# (on ne voudrait pas qu'il saute en allant dans la direction
					# opposée)
					if to_global(direction.normalized()).dot(collision_point.normalized()) >0.8:
						# Enfin, dernier test, on vérifie que le collider touché n'est pas
						# dans un mur, pour celà on utilise le ray_cast_checker :
						# ce ray_cast vérifie qu'aucun mur n'est présent :
						#
						# Joueur  Potentiel mur
						#   ___    |
						#  |___|   |
						#    |  Ray|cast_checker
						#    | ----|-> Raycast
						#   / \    | ___|_________
						#               v         | < - Escalier (?)
						#
						ray_cast_checker.cast_to = stair_ray_cast.translation
						ray_cast_checker.enabled = true
						ray_cast_checker.force_raycast_update()
						# Si le raycast_checker ne touche rien, alors pas de mur,
						# on monte le joueur de la hauteur de la marche.
						# En utilisant un tween pour rendre le mouvement plus fluide
						if not ray_cast_checker.is_colliding():
							var new_translation = Vector3(translation.x,translation.y + ((collision_point_y - ground_y) + 0.4),translation.z)
							#tween.interpolate_property(self,"translation",translation,new_translation,0.08,tween.TRANS_LINEAR,tween.EASE_IN_OUT)
							#tween.start()
							translation = new_translation
							break

# Gestion des évènements de souris, qui ne peuvent être récupérés qu'avec la fonction
# _input.
func _input(event):
	if is_network_master():
		if event.is_action_pressed("next_weapon"):
			equip_weapon(equipped_weapon_index + 1)
		elif event.is_action_pressed("previous_weapon"):
			equip_weapon(equipped_weapon_index - 1)
		if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			pivot.rotation_degrees.x -= event.relative.y * mouse_sensitivity
			self.rotate_y(deg2rad(event.relative.x * mouse_sensitivity * -1))
			
			var camera_rot = pivot.rotation_degrees
			camera_rot.x = clamp(camera_rot.x, -70,70)
			pivot.rotation_degrees = camera_rot
	
# Fonction de saut
func jump():
	if is_on_floor():
		if Input.is_action_just_pressed("jump"):
			velocity.y = JUMP_SPEED
			jumping = true
			
########## Armes ##########
		
# Equipe l'arme weapon_index de l'inventaire
func equip_weapon(weapon_index):
	equipped_weapon_index = clamp(weapon_index, -1, inventory.size()-1)
	if equipped_weapon != null:
		equipped_weapon.hide()
	if equipped_weapon!=null and (inventory[equipped_weapon_index]!=equipped_weapon or equipped_weapon_index==-1):
		NetworkManager.send_deleting_node(equipped_weapon.get_path(), get_tree().get_network_unique_id(), 1)
		rpc("set_equipped_weapon", ["", -1])
	if equipped_weapon_index == -1:
		equipped_weapon = null
	else:
		inventory[equipped_weapon_index].show()
		equipped_weapon = inventory[equipped_weapon_index]
		equipped_weapon.switch_to()
		
		NetworkManager.send_node(equipped_weapon.get_filename(), pivot.get_path(), equipped_weapon.get_name(), [2, get_tree().get_network_unique_id(), equipped_weapon_index])
		rpc("set_equipped_weapon", [equipped_weapon.get_path(), equipped_weapon_index])

# Ajoute une arme dans le monde à l'inventaire
func add_weapon_to_inventory(weapon):
	#rpc("add_weapon_to_inventory_with_path", weapon.get_path())
	var to_delete = weapon.get_path()
	weapon.get_parent().remove_child(weapon)
	NetworkManager.send_deleting_node(to_delete, get_tree().get_network_unique_id(), 0)
	pivot.add_child(weapon)
	rpc_id(1, "add_weap_to_inventory", weapon.get_filename(), pivot.get_path(), weapon.get_name())
	weapon.hide()
	weapon.global_transform = weapon_reference.global_transform
	inventory.append(weapon)

# Lâche l'arme équippée
func drop(weapon_index):
	var weapon = inventory[weapon_index]
	var position = weapon.global_transform.origin
	pivot.remove_child(weapon)
	#get_tree().current_scene.add_child(weapon)
	get_tree().get_root().get_node(TREE_WROLD).add_child(weapon)
	weapon.global_transform.origin = position
	weapon.global_transform.origin.x += 10
	inventory.remove(weapon_index)
	equipped_weapon_index = -1
	if weapon.has_method("drop"):
		weapon.drop()
	equipped_weapon = null
	rpc("drop", weapon_index)
	
func get_weapon(weapon_name):
	for w in inventory:
		if w.item_name == weapon_name:
			return w
	return null
	
########## Dégats et knockback ##########
	
# Prend un coup. Si blood_bool est vrai, alors du sang sera affiché à la position
# hit_position
func take_hit(damage, info, blood_bool=true, hit_position=Vector3(0,0,0)):
	last_hit_info = info
	# Changer la vie ...
	# Si le tir vient de mon équipe, je ne prend pas de dégâts
	if info!=null and info["team"] == player_info["team"]:
		# Sauf si il vient de moi, je prend les moitié des dégâts
		if info["username"] == player_info["username"]:
			set_health(health - damage/2)
			#(rpc_id(1, "set_health", health) 
	else:
		set_health(health - damage)
	# Puis afficher des particules de sang à l'endroit de l'impact
	if blood_bool:
		var blood_instanced : CPUParticles = blood.instance()
		# On l'ajout à la scene global pour pas que le sang "suive" le joueur
		#get_tree().current_scene.add_child(blood_instanced)
		get_tree().get_root().get_node(TREE_WROLD).add_child(blood_instanced)
		blood_instanced.translation = hit_position
		# Je précise que les particules doivent emettre, parce que sinon ça fait n'imp...
		blood_instanced.emitting = true
	rpc_id(1, "set_health", health)


# Fonction pour changer le vecteur de knock, c'est à dire le vecteur de "recul"
# lorsque l'on prend une explosion
func knock(new_knock_vector : Vector3):
	knock_vector += new_knock_vector
	
########## Ramassages des objets ##########
	
# Ajoute des munitions de montant ammo à toutes les armes de l'inventaire
func add_ammo(ammo):
	for weapon in inventory:
		if weapon!=null and weapon.has_method("add_ammo"):
			weapon.add_ammo(ammo)

# Fonction qui tue le personnage. Pour l'instant, cache simplement le modèle
remote func die(info):
	#self.hide()
	#emit_signal("dead",info)
	death_count += 1
	print('died ' + str(death_count) + ' times')

# Connection du signal pour la zone de ramassage.
func _on_PickupArea_entered(area):
	if area.is_in_group("WeaponPickupArea"):
		var weapon = area.get_parent().get_parent()
		if "is_on_ground" in weapon:
			if weapon.is_on_ground == true:
				weapon.is_on_ground = false
				if "is_pickable" in weapon:
					if weapon.is_pickable == true:
						weapon.is_pickable = false
						weapon.pick_up(self)
						if not weapon.is_queued_for_deletion():
							add_weapon_to_inventory(weapon)
							
	elif area.is_in_group("ItemPickupArea"):
		if area.get_parent().get_parent().has_method("pick_up"):
			area.get_parent().get_parent().pick_up(self)

remote func set_equipped_weapon(path):
	return#pour des rpc, n'affecte pas le joueur principal
	
remote func rebirth():
	self.show()
	set_health(100)
