extends Node

var IP_SERVER = "91.121.81.74"
var PORT_SERVER = 5454
var tcp
var VERSION = "ALPHA"

signal new_received_message(message)

enum {AUTH, SIGN_IN, CREATE_LOBBY, JOIN_LOBBY, LOG_OUT, AUTH_OK,
AUTH_WRONG_VERSION, AUTH_WRONG_LOGIN, AUTH_WRONG_PWD, AUTH_ALREADY_OPENED_SAME_ADSRESS,
AUTH_ALREADY_OPENED_SAME_LOGIN, CREATE_PROFILE_OK, CREATE_PROFILE_NOT_OK, CREATE_LOBBY_OK, CREATE_LOBBY_FULL,
CREATE_LOBBY_NAME_ALREADY_USED}

# Called when the node enters the scene tree for the first time.
func _ready():
	tcp = StreamPeerTCP.new()
	tcp.connect_to_host(IP_SERVER, PORT_SERVER)

func _process(_delta):
	if tcp and tcp.is_connected_to_host():
		if tcp.get_available_bytes() > 0:
			var received_message = tcp.get_utf8_string(tcp.get_available_bytes())
			var position = received_message.find(":")

			var split_message_key = received_message.substr(0, position)
			var split_message_value = received_message.substr(position + 1, len(received_message))
			var dict_message = {}
			dict_message["flag"] = split_message_key
			dict_message["content"] = split_message_value
			emit_signal("new_received_message", dict_message)

# envoyer un message chez le serveur d'accès
func send_message(message):
	var arr = message.to_utf8()
	var new_message = tcp.put_data(arr)

# envoyer le message au serveur d'accès authentification ou inscription
# type = AUTH : authentification
# type = SIGN_IN : inscription
func send_auth(login, password, type):
	var message = str(type) + ':' + login + ':' + password.sha256_text() + ':'+ VERSION + '\n'
	send_message(message)

# client clique sur créer lobby
# name_lobby : recupéré de label, entré par client
# nb_players : récupéré de spinBox, entré par client
func send_create_lobby(name_lobby, nb_players):
	var message = str(CREATE_LOBBY) + ':' + name_lobby + ':' + nb_players + '\n'
	send_message(message)

# client appuie sur le button rejoindre un lobby
func send_join_lobby():
	var message = str(JOIN_LOBBY) + ':' + '\n'
	send_message(message)

# client clique sur le button se déconnecter
func send_log_out():
	var message = str(LOG_OUT) + ':' + '\n'
	send_message(message)
	get_tree().quit()
