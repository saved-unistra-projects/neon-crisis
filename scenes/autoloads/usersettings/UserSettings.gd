extends Node

#### Signaux ####
signal setting_changed(setting_path)

const FILE_NAME = "user://user_settings.json"
const SETTINGS_SCENE = "res://scenes/gui/menus/settingsmenu/SettingsMenu.tscn"

var settings = {}

func _ready():
	load_settings()
	var settings_menu = load(SETTINGS_SCENE).instance()
	get_tree().current_scene.add_child(settings_menu)
	settings_menu.call_deferred("startup_init")
	

func save_settings():
	var file = File.new()
	file.open(FILE_NAME, File.WRITE)
	file.store_string(to_json(settings))
	file.close()

func load_settings():
	var file = File.new()
	if file.file_exists(FILE_NAME):
		file.open(FILE_NAME, File.READ)
		var data = parse_json(file.get_as_text())
		file.close()
		if typeof(data) == TYPE_DICTIONARY:
			settings = data
		else:
			printerr("Error")
	else:
		printerr("No data")
		
func remove_settings():
	settings = {}
		
func set_setting(setting_path, value):
	var path_array = get_key_from_path(setting_path, false)
	if path_array != null:
		var path = path_array[0]
		var last_key = path_array[1]
		path[last_key] = var2str(value)
		emit_signal("setting_changed",setting_path)
		
func get_setting(setting_path):
	var result = get_key_from_path(setting_path)
	if result != null and result is String:
		return str2var(get_key_from_path(setting_path))
	else:
		return null
		
func get_key_from_path(setting_path, return_only_value = true):
	var substrings = setting_path.split("/",false)
	if not substrings.empty():
		var dict = settings
		var last_key
		if not return_only_value:
			last_key = substrings[substrings.size()-1]
			substrings.remove(substrings.size()-1)
		var i = substrings.size()
		for string in substrings:
			if not string in dict.keys():
				if i != 1:
					dict[string] = {}
				else:
					dict[string] = null
			dict = dict[string]
			i -= 1
		if not return_only_value:
			return [dict, last_key]
		else:
			return dict
			
func remove_setting(setting_path):
	set_setting(setting_path,null)
