extends Control

#### Attributs ####
onready var in_game_menu = $MenuHolder
onready var hud = $HUD
onready var panel = $Panel
var menu_displayed = false

var out_of_game = true

func _ready():
	disable()

func enable():
	out_of_game = false
	show()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	set_process_input(true)
	resume()
	
func disable():
	out_of_game = true
	hide()
	set_process_input(false)
	in_game_menu.clear_menu()
	panel.hide()
	
func _input(event):
	if visible and event is InputEventKey and not event.is_echo() and event.pressed and event.scancode == KEY_ESCAPE:
			toggle_menu()
			
func toggle_menu():
	if menu_displayed:
		resume()
	else:
		display_menu()
		
func resume():
	in_game_menu.clear_menu()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	hud.show()
	panel.hide()
	menu_displayed = false
	
func display_menu():
	in_game_menu.restore_first_menu()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	hud.hide()
	panel.show()
	menu_displayed = true
		
# Simple fonction du mode de souris (capturé/pas capturé)
func toggle_mouse_mode():
	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
func toggle_crosshair():
	if hud.visible:
		hud.hide()
	else:
		hud.show()
		
func clear():
	hud.propagate_call("clear")
