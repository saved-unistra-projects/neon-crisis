extends Control

#### Attributs ####

onready var hud_elements = $MarginContainer/HUDElements

var elements = {}


# Called when the node enters the scene tree for the first time.
func _ready():
	for node in hud_elements.get_children():
		elements[node.name] = node


func get_element(element_name : String) -> Node :
	assert(element_name in elements.keys())
	return elements[element_name]
