extends "res://scenes/gui/menus/settingsmenu/setting/Setting.gd"

#### Attributs #####
var is_dragged = false
var can_be_dragged = false

var mouse_offset = Vector2.ZERO

var default_value = null

func _ready():
	set_process(false)
	default_value = self.rect_global_position

func _on_HUDElement_gui_input(event):
	if can_be_dragged and event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.is_pressed():
			mouse_offset = self.rect_global_position - get_viewport().get_mouse_position()
			set_process(true)
		else:
			is_dragged = false 
			mouse_offset = Vector2.ZERO
			set_process(false)
			
func _process(_delta):
	var val = get_viewport().get_mouse_position() + mouse_offset
	set_setting_value(val)
	
func set_setting_value(val):
	if val == null and default_value != null:
		set_setting_value(default_value)
	.set_setting_value(val)
	if setting_value != null:
		self.rect_global_position = setting_value
	
func set_draggable(b):
	can_be_dragged = b
