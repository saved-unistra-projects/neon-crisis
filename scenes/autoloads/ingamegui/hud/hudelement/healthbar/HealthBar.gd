extends "res://scenes/autoloads/ingamegui/hud/hudelement/HUDElement.gd"


#### Attributs ####

onready var health_label = $ReferenceRect/HealthLabel

var value = 0 setget set_health

func set_health(new_value):
	value = new_value
	health_label.text = tr("Health") + " : " + str(value)

func clear():
	value = 0
	health_label.text = ""
