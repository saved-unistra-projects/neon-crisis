extends "res://scenes/autoloads/ingamegui/hud/hudelement/HUDElement.gd"

#### Attributs ####
onready var ammo_label = $Label

var weapon_name : String = "" setget set_weapon_name
var current_ammo : int = 0 setget set_current_ammo
var max_ammo : int  = 0 setget set_max_ammo

func set_weapon_name(new_name):
	weapon_name = new_name
	update_label()
	
func set_current_ammo(ammo):
	current_ammo = ammo
	update_label()

func set_max_ammo(ammo):
	max_ammo = ammo
	update_label()
	
func update_label():
	if weapon_name != "":
		ammo_label.text = weapon_name + "\n" +str(current_ammo) + "/" + str(max_ammo)
	
func clear():
	ammo_label.text = ""
	current_ammo = 0
	max_ammo = 0
	weapon_name = ""
	
